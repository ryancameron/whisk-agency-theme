
# [Whisk](http://eggbeater.ca/whisk)
[![Build Status](https://travis-ci.org/roots/sage.svg)](https://travis-ci.org/roots/sage)
[![devDependency Status](https://david-dm.org/roots/sage/dev-status.svg)](https://david-dm.org/roots/sage#info=devDependencies)

Whisk is a WordPress starter theme based on HTML5 Boilerplate, gulp, Bower, and Bootstrap Sass, that will help you make better themes.

* Source: [https://bitbucket.org/eggbeatercreative/whisk)
* Homepage: [http://eggbeater.ca/whisk)
* Documentation: [https://roots.io/sage/docs/](https://roots.io/sage/docs/)

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 5.4.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| Node.js 0.12.x  | `node -v`    | [nodejs.org](http://nodejs.org/) |
| gulp >= 3.8.10  | `gulp -v`    | `npm install -g gulp` |
| Bower >= 1.3.12 | `bower -v`   | `npm install -g bower` |

## Features

* [gulp](http://gulpjs.com/) build script that compiles both Sass and Less, checks for JavaScript errors, optimizes images, and concatenates and minifies files
* [BrowserSync](http://www.browsersync.io/) for keeping multiple browsers and devices synchronized while testing, along with injecting updated CSS and JS into your browser while you're developing
* [Bower](http://bower.io/) for front-end package management
* [asset-builder](https://github.com/austinpray/asset-builder) for the JSON file based asset pipeline
* [Bootstrap](http://getbootstrap.com/)
* [Theme wrapper](https://roots.io/sage/docs/theme-wrapper/)
* ARIA roles and microformats
* Posts use the [hNews](http://microformats.org/wiki/hnews) microformat
* [Multilingual ready](https://roots.io/wpml/) and over 30 available [community translations](https://github.com/roots/sage-translations)
* A Child Theme for creating Client specific itterations of the theme-wrapper
* Advanced control of styling elements using the WordPress Customizer & Kirki

Whisk comes with the [Soil](https://github.com/roots/soil) plugin installed to enable additional features:

* Cleaner output of `wp_head` and enqueued assets
* Cleaner HTML output of navigation menus
* Root relative URLs
* Nice search (`/search/query/`)
* Google CDN jQuery snippet from [HTML5 Boilerplate](http://html5boilerplate.com/)
* Google Analytics snippet from [HTML5 Boilerplate](http://html5boilerplate.com/)

___

## Getting Started (For DEVs)

For a new project:
Login to Bitbucket (you probably already know that if you're looking at this README)
Fork this Repo and give your new repo the name of the project you are working on.

### Setting up your local environment

Create a local database for your new project at http://localhost:8888/MAMP/index.php?page=phpmyadmin&language=English

Clone your new repo:
```
git clone git@bitbucket.org:eggbeatercreative/YOURTHEME.git
```
*If you wish to change the name of the folder that git clones into then add the new folder name at the end of the git command like this. Otherwise the folder will have the same name as your git repo. This is recommended if your git repo name has dashes or underscores in it or if the name is excessively long*
```
git clone git@bitbucket.org:eggbeatercreative/YOURPROJECT.git NEWFOLDERNAME
```
Open the manifest.json file in wp-content/themes/whisk-child:
Change the devurl to the devurl where your site will be located eg for MAMP: http://localhost:8888/YOURPROJECT/

In terminal Navigate to the parent theme folder:
```
cd /wp-content/themes/whisk
```
Then run:
```
sudo npm install
bower install
gulp && gulp --production
```
Next in terminal navigate to the whisk-child folder:
```
cd ..
cd whisk-child
sudo npm install
bower install
gulp
gulp --production
```
In a web browser go to http://localhost:8888/YOURPROJECT

This will prompt the WordPress setup process. Enter in the sitename and your database information for your local environment.

Once this step is complete navigate to the Plugins area and activate the WP Migrate DB and WP Migrate DB plugins. After these have been activated go to Tools/WP Migrate and click on the Settings tab. On the settings page enter in the License key in the "Your License" box. The License key can be found in Vortex.


### Setting up your staging environment

If you are working on an existing project you can skip ahead to the "Syncing Your Environments"

In terminal connect to the eggbeatercloud:

Login to Inmotion AMP at inmotionhosting.com and click on the cPanel link from the selection of Icons on the screen.

In cPanel click on the subdomains icon. If you cannot find the icon you can use the search box at the top of the page to search for the icon.

On the subdomains page enter in the name of your project into the subdomain box.

Once you are satisfied that these settings are correct click the "Create" button.


#### Setting up your wordmove file and pushing your initial files to staging

On your local machine open the Movefile.yaml file in the root of your repo and change the references to whisk to the name of your theme.

EG for your local environment settings
```
local:
  vhost: "http://localhost:8888/whisk"
  WordPress_path: "/Users/User/repos/whisk" # use an absolute path here
```
to
```
local:
  vhost: "http://localhost:8888/YOURPROJECT"
  WordPress_path: "/Users/User/repos/YOURPROJECT" # use an absolute path here
```
Do the same for your staging environment. Keep in mind the name of the domain and file name you setup for your staging environment within Inmotion.

```
staging:
	  vhost: "http://YOURPROJECT.eggbeatercloud.com"
	  WordPress_path: "/home/eggbea5/public_html/YOURPROJECT" # use an absolute path here
```
In Terminal navigate to the root of your repo and run

```
wordmove push -e staging -wupt
```

#### Installing WordPress

In a web browser go to http://YOURPROJECT.eggbeatercloud.com

This will prompt the WordPress setup process. Enter in the sitename and your database information for your staging environment.

Once this step is complete navigate to the Plugins area and activate the WP Migrate DB and WP Migrate DB plugins. After these have been activated go to Tools/WP Migrate and click on the Settings tab. On the settings page enter in the License key in the "Your License" box. The License key can be found in Vortex.

At this point you will want to activate any other plugins that you intend to use as well. Most importantly you will need to activate Kirki, Wordfence, Soil, Yoast SEO, Advanced Custom Fields Pro and Visual Composer. Both Advanced Custom Fields Pro and Visual Composer will need to be registered. Registration keys for each of these plugins are located in Vortex.

You may also want to activate some of the other plugins included in this package. These include:

* ACF-Content Analysis for Yoast SEO - Allows Yoast SEO to analyze content entered in Advanced Custom Fields
* Contact Form 7 - Use this plugin for simple contact formats
* Gravity Forms - For complex forms (license key in Vortex)
* If Menu - Conditional logic for menu items
* Snazzy Maps - Google Maps color palettes
* Widget CSS Classes - Add classes to your widgets so that they can be styled using CSS
* Widget Logic - Conditional logic for placement of widgets (eg only on home, or only on posts)
* WooCommerce - eCommerce plugin for WordPress
* WP Google Maps - Easily integrate google maps into your page layouts using shortcodes
* WYSIWYG Widgets / Widget Blocks - Adds Wyswig blocks that can be added to Widget areas

Next, navigate to Appearance/Themes and check that the child theme for your project is activated.

At this point you are ready to start adding your pages and menu items. If you would like to utilize the Style Guide for this theme all you need to do is create a page called Style Guide. This will pull in a template with all of the common elements for the theme.

## Syncing your environments

In a web browser navigate to http://YOURTHEME.eggbeatercloud.com/wp-admin
Login and navigate to Tools/WP Migrate DB

Click on the settings tab and copy the text in the "Connection Info" box. It should look something like this:
```
https://whisk.eggbeatercloud.com
cRf656IB5hDSQ/6N8W2/HeyU4aNapuhvOL/U42Ca
```
You should also make sure that the "Pull" button under Permissions is set to On.

In a new browser tab navigate to http://localhost:8888/YOURTHEME/wp-admin
Login and navigate to Tools/WP Migrate DB

Click on the "Pull" radio button and paste the connection info you copied from your staging environment into the "Connection info" box.

Make sure that the Backup DB option is set to yes and that the Media Files option is set to on.

Click the "Pull" button and wait for the pull to be finished.

## Working on your local machine using git/gulp

Once you have pulled down the database for your project from the Staging server you are ready to start working on your theme.

To begin navigate to the root project folder (where this README.md is located) in terminal.

For a brand new project:
```
git checkout -b develop
git push -u origin develop
git checkout -b yourfeature
```
For an existing project:
```
git checkout develop
git pull origin develop
git checkout -b yourfeature
```
In another Terminal window or tab navigate to your child theme folder and run:
```
gulp && gulp watch
```
___

## Git workflow:

Make your changes, and when you are ready to merge you changes into develop run:
```
git pull origin develop
git checkout develop
git pull origin develop
git merge yourfeature
git push origin develop
```
If you need to backup your feature branch to the remote origin while working on it run:
```
git push -u origin yourfeature
```
## Adding packages via bower:

from wp-content/themes/whisk-child

Stop any running tasks eg gulp watch (ctrl-c)
```
bower install example-package --save
```
 *if your are adding a bower package that includes images or files with it you will need to create an override for the file location in the bower.json, see the font-awesome override for the proper syntax.*
```
gulp
gulp watch
```
## SASS structure
The style sheets for this theme are created with SASS. They are located in assets/styles and are broken down into folders for common, components and layouts.

In common you will find style sheets for global and variables. In global you should add any styles that apply to the entire site such as H tags, and other element level styling. The variables style sheet contains all of our bootstrap variables. For more info on how to use bootstrap variables see the below section on using variables.

In the components folder you will find style sheets for buttons, comments, forms, woocommerce and wp-classes.

In the layouts folder you will find style sheets for the header, footer, pages, posts, sidebar, woocommerce pages and tinymce. This is where you would add any additional layout files that you need for your theme.

Inside the styles folder you will also find a file called main.scss. This is where all of the above scss files are compiled. To add additional scss files you must add them to this file using the same syntax as the other files referenced in this file.

For more info on using SASS checkout the [SASS docs](http://sass-lang.com/documentation/file.SASS_REFERENCE.html).

## Using Variables

Whisk is setup to use Sass variables. In variables.scss you will find all of the standard bootstrap variables. You can use this area to set your own colors fonts etc. that will apply to the standard bootstrap styling. These variables can also be extended and used in other ways within your theme. For example the most commonly used variables within themes are the color variables. If you set a custom color for Brand Primary you can then use that variable throughout your theme. This saves you from having to enter that color value throughout your theme and also allows you to change all of the references to that color by simply changing its variable.

For example:

```
$brand-primary: #428bca;
```
This variable can then be used in your scss files like so:
```
.my-style {
	background: $brand-primary;
}
```
You can also extend these variables or create new ones for example:
```
$brand-secondary: #bf41c9;
```
This variable can then be used in your scss files like so:
```
.my-style {
	background: $brand-secondary;
}
```

## Using the Customizer

Whisk has also been setup to utilize, the WordPress customizer. In the customizer you can adjust the settings for the Header, Footer, Typography, Layouts, Button/Alert/Panel styles.

For more information on using the Customizer within whisk, simply create a page in your new project called Style Guide. Doing this will trigger the Style Guide page template to be used on this page. Once you navigate to that page you will see an extended description of the use of the customizer within Whisk.

## Making updates to the Whisk theme

If you are making changes to the whisk theme they should be made in the Whisk parent theme

To do this open Terminal and navigate to the whisk theme folder:
```
git checkout develop
git pull origin develop
git checkout -b YOURFEATURE
```
You can now go about making your changes to the theme. Once you have completed your changes run:
```
git pull origin develop
git checkout develop
git merge YOURFEATURE develop
git push origin develop
git checkout master
git merge develop master
git push origin master
```

## Incorporating updates into a forked theme

To incorporate changes into a forked theme, navigate to the project root in terminal and run:
```
git checkout -b whisk-updates
git pull origin develop
git pull git@bitbucket.org:eggbeatercreative/whisk.git
git checkout develop
git rebase whisk-updates develop
git push origin develop
```
Next, in Terminal navigate to the whisk theme folder and run:
```
gulp --production
```
Your site files will now be up to date with whisk and ready for you to continue development as usual.

## Deploying your theme to staging

Once you have completed your changes and are ready to push them to the staging server you will need to package them up into a distribution folder. If you are not in your Child Theme folder navigate there in Terminal then run:

```
gulp --production
```
Once this is completed go to the root of your repo in Terminal and run:
```
wordmove push -e staging -t
```
*Remember that after running gulp --production you will need to run gulp && gulp watch for any changes you make to your child theme to reflect in BrowserSync when you start working on your theme again*

## Deploying your theme to a production environment
