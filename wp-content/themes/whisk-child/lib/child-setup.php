<?php

namespace Whisk\Child\Setup;

use Whisk\Child\Assets;

// Enqueue child stylesheet

function custom_stylesheets() {
wp_enqueue_style('child/css', Assets\asset_path('styles/main.css'), false, null);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\custom_stylesheets');

// Enqueue child JS

function child_scripts() {
wp_enqueue_script('child/js', Assets\asset_path('scripts/child.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts',  __NAMESPACE__ . '\\child_scripts', 9999);
