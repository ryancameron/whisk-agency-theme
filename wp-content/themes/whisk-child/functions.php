<?php



$sage_includes = [
  'lib/child-assets.php',
	'lib/child-setup.php',    // Scripts and stylesheets
];

// Dequeue Parent stylesheet
function parent_stylesheets() {
wp_dequeue_style('sage/css');
}
add_action('wp_enqueue_scripts', 'parent_stylesheets', 101);

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
