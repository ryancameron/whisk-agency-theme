<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/sidebars.php',  // Sidebar setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/wp_bootstrap_navwalker.php', // Bootstrap nav walker
	'lib/menu.php', // Menu functions
  'lib/include-kirki.php', // Include Kirki (checks if kirki plugin is installed. If not it prompts user to install.)
  'lib/class-whisk-kirki.php', // Make sure that user styles will continue to work even if they uninstall the Kirki plugin
  'lib/kirki.php', // Kirki panels, sections, and fields
	'lib/acf-field-groups.php', //ACF field groups
  'lib/google-map-settings.php', // Google Map
  'lib/portfolio-settings.php', // Portfolio custom post type and VC settings
  'lib/testimonials.php', // testimonials custom post type and VC settings
  'lib/employees.php', // employees custom post type and VC settings
  'lib/social-media.php', // employees custom post type and VC settings
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
