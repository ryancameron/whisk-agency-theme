<?php

function page_title_field_groups() {

  acf_add_local_field_group(array(
  	'key' => 'page_title_toggle',
  	'title' => 'Page Title',
  	'fields' => array (
  		array (
  			'key' => 'page_title_1',
  			'label' => 'Page Title',
  			'name' => 'page_title_toggle',
  			'type' => 'select',
        'choices' => array(
          'on'	=> 'on',
          'off' => 'off'
        ),
  		),
  	),
  	'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'page',
        ),
      ),
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'post',
        ),
      ),
  	),
    'position' => 'acf_after_title',
  ));


	acf_add_local_field_group(array(
		'key' => 'page_header_bg',
		'title' => 'Page Header Background Image',
		'fields' => array (
			array (
				'key' => 'page_header_bg_img',
				'label' => 'Page Header Background Image',
				'name' => 'page_header_bg_img',
				'type' => 'image',
			),
		),
	  'location' => array (
	    array (
	      array (
	        'param' => 'post_type',
	        'operator' => '==',
	        'value' => 'page',
	      ),
	    ),
	    array (
	      array (
	        'param' => 'post_type',
	        'operator' => '==',
	       'value' => 'post',
	      ),
	    ),
	  ),
	  'position' => 'acf_after_title',
	));

}//end function


add_action('acf/init', 'page_title_field_groups');
