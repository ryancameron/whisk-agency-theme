<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Social Media',
		'menu_title'	=> 'Social Media',
		'menu_slug' 	=> 'social-media',
		'capability'	=> 'edit_posts',
    'menu_icon'   => 'dashicons-share',
		'redirect'		=> false
	));

}

function social_media_acf_add_local_field_groups() {

  acf_add_local_field_group(array(
  	'key' => 'social_media',
  	'title' => 'Social Media',
  	'fields' => array (
  		array (
  			'key' => 'social_media_repeater',
  			'label' => 'Social Media',
  			'name' => 'social_media',
  			'type' => 'repeater',
        'sub_fields' => array (
                   array (
                       'key' => 'field_icon',
                       'label' => 'Font Awesome Icon',
                       'name' => 'font-awesome-icon-sm',
                       'type' => 'font-awesome',
                       //'field_type' => 'font-awesome-icon',
                       'allow_null' => 0,
                       'load_save_terms' => 0,
                        ),
                    array (
                        'key' => 'field_url',
                        'label' => 'URL',
                        'name' => 'font-awesome-url',
                        'type' => 'text',
                        'allow_null' => 0,
                        'load_save_terms' => 0,
                    ),
  		  ),
      ),
    ),
    'position' => 'acf_after_title',
    'hide_on_screen' => array (
		0 => 'the_content',
	   ),
  'location' => array (
    array (
      array (
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'social-media',
      ),
    ),
  ),

  ));

}//end function

add_action('acf/init', 'social_media_acf_add_local_field_groups');



 ?>
