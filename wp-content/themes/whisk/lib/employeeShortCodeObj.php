function employees_shortcode($atts) {
//ob_start();
extract(shortcode_atts(array(
    'posts' => 10,
  ), $atts));

  ob_start();
    $loop = new WP_Query( array('post_type' => 'employees', 'posts_per_page' => $posts ) );
    $temp_title = '';
    $temp_link = '';
?>
<div id="employees-carousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
<?php
$i = 0;
while ( $loop->have_posts() ) : $loop->the_post();
echo $i;
      $temp_link = get_permalink();
      $image = wp_get_attachment_url( get_post_thumbnail_id() );
      $img = get_field('employee_image');
      //$content = get_field('testimonial_quote');
      $name = get_field('employee_name');
      //$company = get_field('testimonial_company');

if( $i == 0 ){ ?>
   <div class="item active">
<?php  }

 if ($i == 3 || $i == 6 || $i == 9 || $i == 12 || $i == 15){ ?>
  <div class="item">
<?php } ?>

        <div class="col-md-4">
          <div class="employee-img"><img class="employee-photo" src="<?php echo $img['url'] ?>"></div>
          <div class="employee-name"><h4><?php echo $name ?></h4></div>
        </div>

<?php if ($i == 2 || $i == 5 || $i == 7 || $i == 9 || $i == 12 || $i == 15){   ?>
     </div>
<?php }
++$i; ?>
<?php endwhile; ?>


<a class="left carousel-control" href="#employees-carousel" role="button" data-slide="prev">
<i class="fa fa-angle-left" aria-hidden="true"></i>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#employees-carousel" role="button" data-slide="next">
<i class="fa fa-angle-right" aria-hidden="true"></i>
<span class="sr-only">Next</span>
</a>
</div>

 <?php
 wp_reset_query();

	$sc = ob_get_contents();

	/* Clean buffer */
	ob_end_clean();

	/* Return the content as usual */
	return $sc;

}
