<?php
/**
 * Kirki includes
 *
 * The $kirki_includes array determines the code library included in lib/kirki.php.
 * Add or remove files to the array as needed.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$kirki_includes = [
  'lib/kirki_lib/panels_sections.php',          // Kirki panels and sections
  'lib/kirki_lib/header_fields.php',            // Kirki Header fields
  'lib/kirki_lib/look_feel_fields.php',         // Kirki Look and Feel fields
  'lib/kirki_lib/footer_fields.php', // Kirki Footer fields

];

foreach ($kirki_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
