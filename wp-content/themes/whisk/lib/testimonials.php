<?php


add_action( 'init', 'testimonial' );

function testimonial() {

register_post_type( 'testimonial', array(
  'labels' => array(
    'name' => 'Testimonials',
    'singular_name' => 'testimonial',
  ),
  'taxonomies' => array('category'),
  'description' => 'Testimonial items',
  'public' => true,
  'menu_position' => 20,
  'menu_icon'   => 'dashicons-format-quote',
  'supports' => array( 'title', 'editor', 'custom-fields', 'paged', 'page-attributes' )
));
}

function testimonials_acf_add_local_field_groups() {

  acf_add_local_field_group(array(
  	'key' => 'testimonials_item1',
  	'title' => 'Testimonials',
  	'fields' => array (
    array (
      'key' => 'testimonial_image',
      'label' => 'Testimonial Image',
      'name' => 'Testimonial_image',
      'type' => 'image',
    ),
  		array (
  			'key' => 'testimonial_quote_111',
  			'label' => 'Testimonial Quote',
  			'name' => 'testimonial_quote',
  			'type' => 'textarea',
  		),
      array (
        'key' => 'testimonial_name_123',
        'label' => 'Persons Name',
        'name' => 'testimonial_name',
        'type' => 'text',
      ),
    array (
      'key' => 'testimonial_company_456',
      'label' => 'Testimonial Company or Title',
      'name' => 'testimonial_company',
      'type' => 'text',
    ),
  ),
  	'location' => array (
  		array (
  			array (
  				'param' => 'post_type',
  				'operator' => '==',
  				'value' => 'testimonial',
  			),
  		),
  	),
    'position' => 'acf_after_title',
    'hide_on_screen' => array (
    0 => 'the_content',
  ),
  ));

}//end function

add_action('acf/init', 'testimonials_acf_add_local_field_groups');


function testimonial_shortcode($atts) {
//ob_start();

extract(shortcode_atts(array(
    'posts' => 3,
    'testimonialimg' => 'true',
    'testimonialname' => 'true',
    'testimonialcompany' => 'true',
    'cat' => '',
    'order' => 'ASC'
  ), $atts));

    $output = '';


//, 'category_name' => $cat


//'meta_key'			=> 'featured',
//	'orderby'			=> 'meta_value_num',
  /*  $loop = new WP_Query( array('post_type' => 'testimonial', 'posts_per_page' => $posts, 'category_name' => $cat, 'meta_key' => 'featured', 'orderby' => $menuorder, 'order' => $order ) ); */
    $loop = new WP_Query( array('post_type' => 'testimonial', 'posts_per_page' => $posts, 'category_name' => $cat, 'orderby' => 'menu_order', 'order' => $order ) );

//print_r($loop);

    $temp_title = '';
    $temp_link = '';

    $output .= '<div id="testimonials-carousel" class="carousel slide" data-ride="carousel">';
    $output .= '<div class="carousel-inner" role="listbox">';

      $i = 0;

    while ( $loop->have_posts() ) : $loop->the_post();
      $temp_link = get_permalink();
      $image = get_field('testimonial_image');
      $content = get_field('testimonial_quote');
      $name = get_field('testimonial_name');
      $company = get_field('testimonial_company');

        if( $i == 0){
          $output .= '<div class="item active">';
        }else{
          $output .= '<div class="item">';
        }

      //$output .= '<div class="item active">';
      $output .= '<div class="testimonial-content">'.$content.'</div>';

        if($testimonialimg == "true"){
          if($image == !null){
              $output .= '<div class="testimonials-img-holder '. $testimonialimg . '"><img src="'. $image['url'] .'" class="testimonial-img" alt="'.$image['alt'].'"></div>';
          }
        }
        if($testimonialname == "true"){
          $output .= '<div class="testimonial-name"><h4>'.$name.'</h4></div>';
        }
        if($testimonialcompany == "true"){
              $output .= '<div class="testimonial-company '.$testimonialcompany.'"><p>'.$company.'</p></div>';
        }
      $output .= '</div>';
        $i = +1;
    endwhile;

    $output .= '</div>';
$output .= '<a class="left carousel-control" href="#testimonials-carousel" role="button" data-slide="prev">';
$output .= '<i class="fa fa-angle-left" aria-hidden="true"></i>';
$output .= '<span class="sr-only">Previous</span>';
$output .= '</a>';
$output .= '<a class="right carousel-control" href="#testimonials-carousel" role="button" data-slide="next">';
$output .= '<i class="fa fa-angle-right" aria-hidden="true"></i>';
$output .= '<span class="sr-only">Next</span>';
$output .= '</a>';
$output .= '</div>';

 wp_reset_query();
 //wp_reset_postdata();
 //return ob_get_clean();
    return $output;
}

add_shortcode( 'testimonial', 'testimonial_shortcode' );

add_action( 'vc_before_init', 'testimonial_integrateWithVC' );

function testimonial_integrateWithVC() {
   vc_map( array(
      "name" => __( "Testimonials Slider", "Testimonial Pieces" ),
      "base" => "testimonial",
      "class" => "",
      "category" => __( "Content", "Testimonial-pieces"),
      //'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      //'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
       "params" => array(
         array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => __( "Number of Testimonials", "my-text-domain" ),
            "param_name" => "posts",
            "value" => __( "3", "my-text-domain" ),
            "description" => __( "Enter the number of testimonials you want in this section. Default is 3.", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Testimonial Image", "Testimonial-pieces" ),
            "param_name" => "testimonialimg",
            "value" => array(
              __("on") => "true",
              __("off") => "false",
            ),
            "description" => __( "Turn image on or off", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Name of person giving the testimonial", "my-text-domain" ),
            "param_name" => "testimonialname",
            "value" => array(
              __("on") => 'true',
              __("off") => 'false',
            ),
            "description" => __( "Turn name on or off", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Company Name", "my-text-domain" ),
            "param_name" => "testimonialcompany",
            "value" => array(
              __("on") => 'true',
              __("off") => 'false',
            ),
            "description" => __( "Turn company name on or off", "my-text-domain" )
         ),
         array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Category", "my-text-domain" ),
            "param_name" => "cat",
            "value" => __( "", "my-text-domain" ),
            "description" => __( "Select Specific Categories, seperated by a comma", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Category", "my-text-domain" ),
            "param_name" => "order",
            "value" => array(
              __("ASC") => 'ASC',
              __("DESC") => 'DESC',
            ),
            "description" => __( "Order by ASC or DESC", "my-text-domain" )
         ),

      ),
      'save_always' => true,
   ) );
}


function testimonial_columns_shortcode($atts) {
//ob_start();

extract(shortcode_atts(array(
    'col' => '4',
    'posts' => 3,
    'testimonialimg' => 'true',
    'testimonialname' => 'true',
    'testimonialcompany' => 'true',
    'cat' => '',
    'order' => 'ASC'
  ), $atts));

    $output = '';

    $loop = new WP_Query( array('post_type' => 'testimonial', 'posts_per_page' => $posts, 'category_name' => $cat, 'orderby' => 'menu_order', 'order' => $order ) );

//print_r($loop);

    $temp_title = '';
    $temp_link = '';

    //$output .= '<div class="carousel slide" data-ride="carousel">';
    $output .= '<div class="row testimonial-columns">';


    while ( $loop->have_posts() ) : $loop->the_post();
      $temp_link = get_permalink();
      $image = get_field('testimonial_image');
      $content = get_field('testimonial_quote');
      $name = get_field('testimonial_name');
      $company = get_field('testimonial_company');


      $output .= '<div class="col-md-';
      if($col == '12'){
        $output .= '12';
      }else if($col == '6'){
        $output .= '6';
      }else{
        $output .= '4';
      }
      $output .='" >'; //close col-md class

        $output .= '<div class="testimonial-content">'.$content.'</div>';

        if($testimonialimg == "true"){
          if($image == !null){
              $output .= '<div class="testimonials-img-holder '. $testimonialimg . '"><img src="'. $image['url'] .'" class="testimonial-img" alt="'.$image['alt'].'"></div>';
          }
        }

        if($testimonialname == "true"){
          $output .= '<div class="testimonial-name"><h4>'.$name.'</h4></div>';
        }
        if($testimonialcompany == "true"){
              $output .= '<div class="testimonial-company '.$testimonialcompany.'"><p>'.$company.'</p></div>';
        }

      $output .= '</div>';
      endwhile;

    $output .= '</div>';

 wp_reset_query();
 return $output;
}

add_shortcode( 'testimonial_columns', 'testimonial_columns_shortcode' );

add_action( 'vc_before_init', 'testimonial_col_integrateWithVC' );

function testimonial_col_integrateWithVC() {
   vc_map( array(
      "name" => __( "Testimonials Columns", "Testimonial Pieces" ),
      "base" => "testimonial_columns",
      "class" => "",
      "category" => __( "Content", "Testimonial-pieces"),
      //'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      //'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
       "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Number of Columns", "Testimonial-pieces" ),
            "param_name" => "col",
            "value" => array(
              __("4") => "3",
              __("6") => "2",
              __("12") => "1",
            ),
            "description" => __( "select number of columns", "my-text-domain" )
         ),

         array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => __( "Number of Testimonials", "my-text-domain" ),
            "param_name" => "posts",
            "value" => __( "3", "my-text-domain" ),
            "description" => __( "Enter the number of testimonials you want in this section. Default is 3.", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Testimonial Image", "Testimonial-pieces" ),
            "param_name" => "testimonialimg",
            "value" => array(
              __("on") => "true",
              __("off") => "false",
            ),
            "description" => __( "Turn image on or off", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Name of person giving the testimonial", "my-text-domain" ),
            "param_name" => "testimonialname",
            "value" => array(
              __("on") => 'true',
              __("off") => 'false',
            ),
            "description" => __( "Turn name on or off", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Company Name", "my-text-domain" ),
            "param_name" => "testimonialcompany",
            "value" => array(
              __("on") => 'true',
              __("off") => 'false',
            ),
            "description" => __( "Turn company name on or off", "my-text-domain" )
         ),
         array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Category", "my-text-domain" ),
            "param_name" => "cat",
            "value" => __( "", "my-text-domain" ),
            "description" => __( "Select Specific Categories, seperated by a comma", "my-text-domain" )
         ),

         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Category", "my-text-domain" ),
            "param_name" => "order",
            "value" => array(
              __("ASC") => 'ASC',
              __("DESC") => 'DESC',
            ),
            "description" => __( "Order by ASC or DESC", "my-text-domain" )
         ),

      ),
      'save_always' => true,
   ) );
}


?>
