<?php
/**
 * Register sidebars
 */

function widgets_init() {
	 $sidebarstyle = get_theme_mod( 'w_setting', 'option-1');
	 $sidebarpanelcolor = get_theme_mod('w_setting_color');

	if ($sidebarstyle =='option-1'){
 		register_sidebar( array(
 			'name'          => __('Primary', 'sage'),
 			'id'            => 'sidebar-primary',
 			'before_widget' => '<section id="%1$s" class="widget %1$s %2$s">',
 			'after_widget'  => '</section>',
 			'before_title'  => '<h3>',
 			'after_title'   => '</h3>',
 		));
 	}
	else if ($sidebarstyle =='option-2' && $sidebarpanelcolor == 'default') {
		  register_sidebar(array(
			  'name'          => __('Primary', 'sage'),
			  'id'            => 'sidebar-primary',
			  'before_widget' => '<section id="%1$s" class="widget %1$s %2$s panel panel-default">',
			  'after_widget'  => '</section>',
			  'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
			  'after_title'   => '</div></h3>'
		  ));
	  }
		else if ($sidebarstyle =='option-2' &&  $sidebarpanelcolor == 'primary') {
		  register_sidebar(array(
			  'name'          => __('Primary', 'sage'),
			  'id'            => 'sidebar-primary',
			  'before_widget' => '<section id="%1$s" class="widget %1$s %2$s panel panel-primary">',
			  'after_widget'  => '</section>',
			  'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
			  'after_title'   => '</div></h3>'
		  ));
	  }
		else if ($sidebarstyle =='option-2' &&  $sidebarpanelcolor == 'success') {
		  register_sidebar(array(
			  'name'          => __('Primary', 'sage'),
			  'id'            => 'sidebar-primary',
			  'before_widget' => '<section id="%1$s" class="widget %1$s %2$s panel panel-success">',
			  'after_widget'  => '</section>',
			  'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
			  'after_title'   => '</div></h3>'
		  ));
	  }
		else if ($sidebarstyle =='option-2' &&  $sidebarpanelcolor == 'warning') {
		  register_sidebar(array(
			  'name'          => __('Primary', 'sage'),
			  'id'            => 'sidebar-primary',
			  'before_widget' => '<section id="%1$s" class="widget %1$s %2$s panel panel-warning">',
			  'after_widget'  => '</section>',
			  'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
			  'after_title'   => '</div></h3>'
		  ));
	  }
		else if ($sidebarstyle =='option-2' &&  $sidebarpanelcolor == 'danger') {
		  register_sidebar(array(
			  'name'          => __('Primary', 'sage'),
			  'id'            => 'sidebar-primary',
			  'before_widget' => '<section id="%1$s" class="widget %1$s %2$s panel panel-danger">',
			  'after_widget'  => '</section>',
			  'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
			  'after_title'   => '</div></h3>'
		  ));
	  }
	else if ($sidebarstyle == 'option-3') {
		register_sidebar( array(
			'name'          => __('Primary', 'sage'),
			'id'            => 'sidebar-primary',
			'before_widget' => '<section id="%1$s" class="widget %1$s %2$s well">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		));
	}

  $footer = get_theme_mod( 'footer_grid');

  if ( $footer == 'option-1'){
    register_sidebar( array(
     'name' => 'Footer 1',
     'id' => 'footer-1',
     'description' => 'Appears in the footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );

  }else if($footer ==  'option-2'){
    register_sidebar( array(
     'name' => 'Footer 1',
     'id' => 'footer-1',
     'description' => 'Appears in the footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );
    register_sidebar( array(
     'name' => 'Footer 2',
     'id' => 'footer-2',
     'description' => 'Appears in the footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );


  }else if($footer ==  'option-3'){
    register_sidebar( array(
      'name' => 'Footer 1',
      'id' => 'footer-1',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 2',
      'id' => 'footer-2',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 3',
      'id' => 'footer-3',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }else if($footer ==  'option-4'){
    register_sidebar( array(
      'name' => 'Footer 1',
      'id' => 'footer-1',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 2',
      'id' => 'footer-2',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 3',
      'id' => 'footer-3',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 4',
      'id' => 'footer-4',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );

  }else if($footer ==  'option-6'){
    register_sidebar( array(
      'name' => 'Footer 1',
      'id' => 'footer-1',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 2',
      'id' => 'footer-2',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 3',
      'id' => 'footer-3',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 4',
      'id' => 'footer-4',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 5',
      'id' => 'footer-5',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 6',
      'id' => 'footer-6',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );

  }else if($footer ==  'option-7' || 'option-8' || 'option-9' || 'option-10'){
    register_sidebar( array(
      'name' => 'Footer 1',
      'id' => 'footer-1',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Footer 2',
      'id' => 'footer-2',
      'description' => 'Appears in the footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );

  }//end else if

  $pre_header = get_theme_mod( 'pre_header_grid');

  if ( $pre_header == 'option-1'){
    register_sidebar( array(
     'name' => 'Pre Header 1',
     'id' => 'pre-header-1',
     'description' => 'Appears in the Pre Header area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );

  }else if($pre_header ==  'option-2'){
    register_sidebar( array(
     'name' => 'Pre Header 1',
     'id' => 'pre-header-1',
     'description' => 'Appears in the Pre Header area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );
    register_sidebar( array(
     'name' => 'Pre Header 2',
     'id' => 'pre-header-2',
     'description' => 'Appears in the Pre Header area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );


  }else if($pre_header ==  'option-3'){
    register_sidebar( array(
      'name' => 'Pre Header 1',
      'id' => 'pre-header-1',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 2',
      'id' => 'pre-header-2',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 3',
      'id' => 'pre-header-3',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }else if($pre_header ==  'option-4'){
    register_sidebar( array(
      'name' => 'Pre Header 1',
      'id' => 'pre-header-1',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 2',
      'id' => 'pre-header-2',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 3',
      'id' => 'pre-header-3',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 4',
      'id' => 'pre-header-4',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );

  }else if($pre_header ==  'option-6'){
    register_sidebar( array(
      'name' => 'Pre Header 1',
      'id' => 'pre-header-1',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 2',
      'id' => 'pre-header-2',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 3',
      'id' => 'pre-header-3',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 4',
      'id' => 'pre-header-4',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 5',
      'id' => 'pre-header-5',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Header 6',
      'id' => 'pre-header-6',
      'description' => 'Appears in the Pre Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }//end else if

  $post_header = get_theme_mod( 'post_header_grid');

  if ( $post_header == 'option-1'){
    register_sidebar( array(
     'name' => 'Post Header 1',
     'id' => 'post-header-1',
     'description' => 'Appears in the Post Header area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );

  }else if($post_header ==  'option-2'){
    register_sidebar( array(
     'name' => 'Post Header 1',
     'id' => 'post-header-1',
     'description' => 'Appears in the Post Header area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );
    register_sidebar( array(
     'name' => 'Post Header 2',
     'id' => 'post-header-2',
     'description' => 'Appears in the Post Header area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );


  }else if($post_header ==  'option-3'){
    register_sidebar( array(
      'name' => 'Post Header 1',
      'id' => 'post-header-1',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 2',
      'id' => 'post-header-2',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 3',
      'id' => 'post-header-3',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }else if($post_header ==  'option-4'){
    register_sidebar( array(
      'name' => 'Post Header 1',
      'id' => 'post-header-1',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 2',
      'id' => 'post-header-2',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 3',
      'id' => 'post-header-3',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 4',
      'id' => 'post-header-4',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );

  }else if($post_header ==  'option-6'){
    register_sidebar( array(
      'name' => 'Post Header 1',
      'id' => 'post-header-1',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 2',
      'id' => 'post-header-2',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 3',
      'id' => 'post-header-3',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 4',
      'id' => 'post-header-4',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 5',
      'id' => 'post-header-5',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Header 6',
      'id' => 'post-header-6',
      'description' => 'Appears in the Post Header area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }//end else if

  $pre_footer = get_theme_mod( 'pre_footer_grid');

  if ( $pre_footer == 'option-1'){
    register_sidebar( array(
     'name' => 'Pre Footer 1',
     'id' => 'pre-footer-1',
     'description' => 'Appears in the Pre Footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );

  }else if($pre_footer ==  'option-2'){
    register_sidebar( array(
     'name' => 'Pre Footer 1',
     'id' => 'pre-footer-1',
     'description' => 'Appears in the Pre Footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );
    register_sidebar( array(
     'name' => 'Pre Footer 2',
     'id' => 'pre-footer-2',
     'description' => 'Appears in the Pre Footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );


  }else if($pre_footer ==  'option-3'){
    register_sidebar( array(
      'name' => 'Pre Footer 1',
      'id' => 'pre-footer-1',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 2',
      'id' => 'pre-footer-2',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 3',
      'id' => 'pre-footer-3',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }else if($pre_footer ==  'option-4'){
    register_sidebar( array(
      'name' => 'Pre Footer 1',
      'id' => 'pre-footer-1',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 2',
      'id' => 'pre-footer-2',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 3',
      'id' => 'pre-footer-3',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 4',
      'id' => 'pre-footer-4',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );

  }else if($pre_footer ==  'option-6'){
    register_sidebar( array(
      'name' => 'Pre Footer 1',
      'id' => 'pre-footer-1',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 2',
      'id' => 'pre-footer-2',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 3',
      'id' => 'pre-footer-3',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 4',
      'id' => 'pre-footer-4',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 5',
      'id' => 'pre-footer-5',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Pre Footer 6',
      'id' => 'pre-footer-6',
      'description' => 'Appears in the Pre Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }//end else if

  $post_footer = get_theme_mod( 'post_footer_grid');

  if ( $post_footer == 'option-1'){
    register_sidebar( array(
     'name' => 'Post Footer 1',
     'id' => 'post-footer-1',
     'description' => 'Appears in the Post Footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );

  }else if($post_footer ==  'option-2'){
    register_sidebar( array(
     'name' => 'Post Footer 1',
     'id' => 'post-footer-1',
     'description' => 'Appears in the Post Footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );
    register_sidebar( array(
     'name' => 'Post Footer 2',
     'id' => 'post-footer-2',
     'description' => 'Appears in the Post Footer area',
     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
     'after_widget' => '</aside>',
     'before_title' => '<h3 class="widget-title">',
     'after_title' => '</h3>',
    ) );


  }else if($post_footer ==  'option-3'){
    register_sidebar( array(
      'name' => 'Post Footer 1',
      'id' => 'post-footer-1',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 2',
      'id' => 'post-footer-2',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 3',
      'id' => 'post-footer-3',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }else if($post_footer ==  'option-4'){
    register_sidebar( array(
      'name' => 'Post Footer 1',
      'id' => 'post-footer-1',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 2',
      'id' => 'post-footer-2',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 3',
      'id' => 'post-footer-3',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 4',
      'id' => 'post-footer-4',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );

  }else if($post_footer ==  'option-6'){
    register_sidebar( array(
      'name' => 'Post Footer 1',
      'id' => 'post-footer-1',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 2',
      'id' => 'post-footer-2',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 3',
      'id' => 'post-footer-3',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 4',
      'id' => 'post-footer-4',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 5',
      'id' => 'post-footer-5',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
    register_sidebar( array(
      'name' => 'Post Footer 6',
      'id' => 'post-footer-6',
      'description' => 'Appears in the Post Footer area',
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="widget-title">',
      'after_title' => '</h3>',
    ) );
  }//end else if

}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');
