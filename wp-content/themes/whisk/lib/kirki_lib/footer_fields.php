<?
function whisk_footer_fields( $fields ) {

  // Footer Container
  $fields[] = array(
    'type'        => 'radio-buttonset',
    'settings'    => 'footer_container',
    'label'       => __( 'Footer Container', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => 'container',
    'priority'    => 10,
    'choices'     => array(
      'container'   => esc_attr__( 'Within Grid', 'whisk' ),
      'container-fluid' => esc_attr__( 'Full Width', 'whisk' ),
    ),
  );

  // Footer Grid
  $fields[] = array(
    'label'       => __( 'Footer Grid', 'translation_domain' ),
    'section'     => 'footer_options',
    'settings'    => 'footer_grid',
    'type'        => 'select',
    'priority'    => 10,
    'choices'     => array(
        'option-1' => esc_attr__( '1 Column', '1' ),
        'option-2' => esc_attr__( '2 Column', '2' ),
        'option-3' => esc_attr__( '3 Column', '3' ),
        'option-4' => esc_attr__( '4 Column', '4' ),
        'option-6' => esc_attr__( '6 Column', '6' ),
        'option-7' => esc_attr__( '2/3 + 1/3', '7' ),
        'option-8' => esc_attr__( '1/3 + 2/3', '8' ),
        'option-9' => esc_attr__( '1/4 + 3/4', '9' ),
        'option-10' => esc_attr__( '3/4 + 1/4', '10' ),
    ),
  );

  // Footer Background Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'footer_color',
    'label'       => __( 'Footer Background Color', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => '#3d4045',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => 'footer',
        'property' => 'background-color',
      ),
    ),
  );

  // Footer Background Image
  $fields[] = array(
      'type'        => 'background',
      'settings'    => 'footer_background_image',
      'label'       => __( 'Footer Background Image', 'whisk' ),
      'section'     => 'footer_options',
      'default'     => array(
          'image'    => '',
          'repeat'   => 'no-repeat',
          'size'     => 'cover',
          'attach'   => 'initial',
          'position' => 'left-bottom',
      ),
      'priority'    => 10,
      'output'      => array(
        array(
          'element' => 'footer',
        ),
      ),
  );



  // Footer Heading Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'footer_heading_color',
    'label'       => __( 'Footer Heading Color', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => '#ffffff',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => 'footer h2, footer h3, footer h4, footer h5, footer h6',
        'property' => 'color',
      ),
    ),
  );

  // Footer Text Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'footer_text_color',
    'label'       => __( 'Footer Text Color', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => '#ffffff',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => 'footer',
        'property' => 'color',
      ),
    ),
  );

  // Footer Link Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'footer_link_color',
    'label'       => __( 'Footer Link Color', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => '#ffffff',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => 'footer a',
        'property' => 'color',
      ),
    ),
  );

  // Footer Link Hover Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'footer_link_hover_color',
    'label'       => __( 'Footer Link Hover Color', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => '#ffffff',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => 'footer a:hover',
        'property' => 'color',
      ),
    ),
  );

  // Footer Link Hover Decoration Alignment
  $fields[] = array(
    'type'        => 'radio-buttonset',
    'settings'    => 'footer_link_hover_decoration',
    'label'       => __( 'Footer Link Hover Decoration', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => 'yes',
    'priority'    => 10,
    'choices'     => array(
      ''   => esc_attr__( 'Yes', 'whisk' ),
      'none' => esc_attr__( 'No', 'whisk' ),
    ),
    'output'      => array(
      array(
        'element' => 'footer a:hover',
        'property' => 'text-decoration',
      ),
    ),
  );

  // Footer Text Alignment
  $fields[] = array(
    'type'        => 'radio-buttonset',
    'settings'    => 'footer_text_alignment',
    'label'       => __( 'Footer Text Align', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => 'left',
    'priority'    => 10,
    'choices'     => array(
      'left'   => esc_attr__( 'Left', 'whisk' ),
      'center' => esc_attr__( 'Center', 'whisk' ),
      'right' => esc_attr__( 'Right', 'whisk' ),
    ),
    'output'      => array(
      array(
        'element' => 'footer',
        'property' => 'text-align',
      ),
    ),
  );


  // Footer Padding
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'footer_padding',
    'label'       => esc_attr__( 'Footer Padding', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => 30,
    'choices'     => array(
      'min'  => '0',
      'max'  => '300',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => 'footer',
        'property' => 'padding-top',
        'units'    => 'px',
      ),
      array(
        'element' => 'footer',
        'property' => 'padding-bottom',
        'units'    => 'px',
      ),
    ),
  );

  // Footer Top Margin
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'footer_margin_top',
    'label'       => esc_attr__( 'Footer Top Margin', 'whisk' ),
    'section'     => 'footer_options',
    'default'     => 30,
    'choices'     => array(
      'min'  => '0',
      'max'  => '300',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => 'footer',
        'property' => 'margin-top',
        'units'    => 'px',
      ),
    ),
  );



  // Toggle Pre Footer
  $fields[] = array(
    'type'        => 'radio-buttonset',
  	'settings'    => 'pre_footer_toggle',
  	'label'       => __( 'Toggle Pre Footer', 'whisk' ),
  	'section'     => 'pre_footer',
  	'default'     => 'hide-pre-footer',
  	'priority'    => 10,
  	'choices'     => array(
  		'hide-pre-footer'   => esc_attr__( 'Off', 'whisk' ),
  		'show-pre-footer' => esc_attr__( 'On', 'whisk' ),
  	),
  );

  // Pre Footer Grid Layout
  $fields[] = array(
    'label'       => __( 'Pre Footer Grid Layout', 'whisk' ),
    'section'     => 'pre_footer',
    'settings'    => 'pre_footer_grid',
    'type'        => 'select',
    'priority'    => 10,
    'choices'     => array(
        'option-1' => esc_attr__( '1 Column', '1' ),
        'option-2' => esc_attr__( '2 Column', '2' ),
        'option-3' => esc_attr__( '3 Column', '3' ),
        'option-4' => esc_attr__( '4 Column', '4' ),
        'option-6' => esc_attr__( '6 Column', '6' ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'pre_footer_toggle',
        'operator' => '==',
        'value' => 'show-pre-footer'
      ),
    ),
  );

  // Pre Footer Background Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'pre_footer_color',
    'label'       => __( 'Pre Footer Background Color', 'whisk' ),
    'section'     => 'pre_footer',
    'default'     => '#3d4045',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.pre-footer',
        'property' => 'background-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'pre_footer_toggle',
        'operator' => '==',
        'value' => 'show-pre-footer'
      ),
    ),
  );

  // Toggle Post Footer
  $fields[] = array(
    'type'        => 'radio-buttonset',
  	'settings'    => 'post_footer_toggle',
  	'label'       => __( 'Toggle Post Footer', 'whisk' ),
  	'section'     => 'post_footer',
  	'default'     => 'hide-post-footer',
  	'priority'    => 10,
  	'choices'     => array(
  		'hide-post-footer'   => esc_attr__( 'Off', 'whisk' ),
  		'show-post-footer' => esc_attr__( 'On', 'whisk' ),
  	),
  );

  // Post Footer Grid Layout
  $fields[] = array(
    'section'     => 'post_footer',
    'label'       => __( 'Post Footer Grid Layout', 'whisk' ),
    'settings'    => 'post_footer_grid',
    'type'        => 'select',
    'priority'    => 10,
    'choices'     => array(
        'option-1' => esc_attr__( '1 Column', '1' ),
        'option-2' => esc_attr__( '2 Column', '2' ),
        'option-3' => esc_attr__( '3 Column', '3' ),
        'option-4' => esc_attr__( '4 Column', '4' ),
        'option-6' => esc_attr__( '6 Column', '6' ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'post_footer_toggle',
        'operator' => '==',
        'value' => 'show-post-footer'
      ),
    ),
  );

  // Post Footer Background Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'post_footer_color',
    'label'       => __( 'Post Footer Background Color', 'whisk' ),
    'section'     => 'post_footer',
    'default'     => '#3d4045',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.post-footer',
        'property' => 'background-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'post_footer_toggle',
        'operator' => '==',
        'value' => 'show-post-footer'
      ),
    ),
  );

	/*
  * Footer Menu fields
  */

  // Toggle Footer Menu
  $fields[] = array(
    'type'        => 'radio-buttonset',
    'settings'    => 'footer_menu_toggle',
    'label'       => __( 'Toggle Footer Menu', 'whisk' ),
    'section'     => 'footer_menu_options',
    'default'     => 'hide-footer-menu',
    'priority'    => 10,
    'choices'     => array(
      'hide-footer-menu'   => esc_attr__( 'Off', 'whisk' ),
      'show-footer-menu' => esc_attr__( 'On', 'whisk' ),
    ),
  );

  // Menu Position
  $fields[] = array(
    'type'        => 'radio-buttonset',
    'settings'    => 'footer_menu_position',
    'label'       => __( 'Footer Menu Postion', 'whisk' ),
    'section'     => 'footer_menu_options',
    'default'     => 'navbar-left',
    'priority'    => 10,
    'choices'     => array(
      'navbar-left'   => esc_attr__( 'Left', 'whisk' ),
      'navbar-center'  => esc_attr__( 'Center', 'whisk' ),
      'navbar-right' => esc_attr__( 'Right', 'whisk' ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'footer_menu_toggle',
        'operator' => '==',
        'value' => 'show-footer-menu'
      ),
    ),
  );

  // Menu Link Typography
  $fields[] = array(
    'type'        => 'typography',
    'settings'    => 'footer_menu_typography',
    'label'       => esc_attr__( 'Menu Typography', 'whisk' ),
    'section'     => 'footer_menu_options',
    'default'     => array(
      'font-family'    => 'Open Sans',
      'variant'        => 'regular',
      'font-size'      => '14px',
      'letter-spacing' => '0',
      'subsets'        => array( 'latin-ext' ),
      'color'          => '#333333',
      'text-transform' => 'none',
      'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
      array(
        'element' => 'footer .navbar-nav > li > a',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'footer_menu_toggle',
        'operator' => '==',
        'value' => 'show-footer-menu'
      ),
    ),
  );

  // Menu Item Text Hover Color
  $fields[] = array(
    'type'        => 'typography',
    'settings'    => 'footer_menu_hover',
    'label'       => esc_attr__( 'Menu Item Text Hover Color', 'whisk' ),
    'section'     => 'footer_menu_options',
    'default'     => array(
      'variant'        => 'regular',
      'color'          => '#cccccc',
    ),
    'priority'    => 10,
    'output'      => array(
      array(
        'element' => 'footer .navbar-nav > li > a:hover',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'footer_menu_toggle',
        'operator' => '==',
        'value' => 'show-footer-menu'
      ),
    ),
  );

  // Menu Item Background Hover Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'footer_menu_hover_background',
    'label'       => esc_attr__( 'Menu Item Background Hover Color', 'whisk' ),
    'section'     => 'footer_menu_options',
    'default'     => 'rgba(0,0,0,0)',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => 'footer .navbar-nav > li > a:hover',
        'property' => 'background-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'footer_menu_toggle',
        'operator' => '==',
        'value' => 'show-footer-menu'
      ),
    ),
  );

  // Active Menu Item Text Color
  $fields[] = array(
    'type'        => 'typography',
    'settings'    => 'footer_menu_active',
    'label'       => esc_attr__( 'Active Menu Item Text Color', 'whisk' ),
    'section'     => 'footer_menu_options',
    'default'     => array(
      'variant'        => 'regular',
      'color'          => '#cccccc',
    ),
    'priority'    => 10,
    'output'      => array(
      array(
        'element' => 'footer .navbar-nav > .active > a, footer .navbar-default .navbar-nav > .active > a:focus',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'footer_menu_toggle',
        'operator' => '==',
        'value' => 'show-footer-menu'
      ),
    ),
  );

  // Active Menu Item Background Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'footer_menu_active_background',
    'label'       => esc_attr__( 'Active Menu Item Background Color', 'whisk' ),
    'section'     => 'footer_menu_options',
    'default'     => '#E7E7E7',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => 'footer .navbar-nav > .active > a, footer .navbar-default .navbar-nav > .active > a:focus, footer .navbar-default .navbar-nav > .active > a:hover',
        'property' => 'background-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'footer_menu_toggle',
        'operator' => '==',
        'value' => 'show-footer-menu'
      ),
    ),
  );


  return $fields;
}
add_filter( 'kirki/fields', 'whisk_footer_fields' );
