<?
function whisk_header_fields( $fields ) {

  // Toggle Sticky Header
  $fields[] = array(
    'type'        => 'radio-buttonset',
  	'settings'    => 'sticky_header',
  	'label'       => __( 'Toggle Sticky Header', 'whisk' ),
  	'section'     => 'header_options',
  	'default'     => 'navbar-static-top',
  	'priority'    => 10,
  	'choices'     => array(
  		'navbar-static-top'   => esc_attr__( 'Off', 'whisk' ),
  		'navbar-fixed-top' => esc_attr__( 'On', 'whisk' ),
  	),
  );

	// Header Width
	$fields[] = array(
		'type'        => 'radio-buttonset',
		'settings'    => 'header_width',
		'label'       => __( 'Header Width', 'whisk' ),
		'section'     => 'header_options',
		'default'     => 'container',
		'priority'    => 10,
		'choices'     => array(
			'container'   => esc_attr__( 'Container - 1170px', 'whisk' ),
			'container-fluid' => esc_attr__( 'Container Fluid - 100%', 'whisk' ),
		),
	);

  // Logo/Branding Position
  $fields[] = array(
    'type'        => 'radio-buttonset',
    'settings'    => 'logo_position',
    'label'       => __( 'Logo/Branding Postion', 'whisk' ),
    'section'     => 'header_options',
    'default'     => 'whisk-navbar-left',
    'priority'    => 10,
    'choices'     => array(
      'whisk-navbar-left'   => esc_attr__( 'Left', 'whisk' ),
      'logo-center'  => esc_attr__( 'Center', 'whisk' ),
      'whisk-navbar-right' => esc_attr__( 'Right', 'whisk' ),
    ),
  );

	//logo width
	  $fields[] = array(
	    'type'        => 'number',
	    'settings'    => 'logo_width',
	    'label'       => __( 'Logo Width', 'whisk' ),
	    'description' => __( 'Set the width to half the size of your logo', 'whisk' ),
	    'section'     => 'header_options',
	    'priority'    => 10,
	    'default'     => '150',
	    'choices'     => array(
	  		'min'  => '0',
	  		'max'  => '500',
	  		'step' => '1',
	  	),
	    'output'      => array(
	      array(
	        'element' => '.desktop a.navbar-brand .desktop-logo',
	        'property' => 'width',
	        'units'    => 'px',
	      ),
	    ),
	  );

	  // Header Logo
	  $fields[] = array(
	    'type'        => 'image',
	  	'settings'    => 'logo_image',
	  	'label'       => __( 'Site Logo', 'whisk' ),
	    'description' => __( 'Upload an image. Make sure you set the width to half the size of the original logo.', 'whisk' ),
	  	'section'     => 'header_options',
	  	'default'     => '',
	  	'priority'    => 10,
	  );

	  //logo width
	    $fields[] = array(
	      'type'        => 'number',
	      'settings'    => 'logo_sticky_width',
	      'label'       => __( 'Logo Width', 'whisk' ),
	      'description' => __( 'Set the width to half the size of your logo', 'whisk' ),
	      'section'     => 'header_options',
	      'priority'    => 10,
	      'default'     => '150',
	      'choices'     => array(
	    		'min'  => '0',
	    		'max'  => '500',
	    		'step' => '1',
	    	),
	      'output'      => array(
	        array(
	          'element' => '.desktop a.navbar-brand .sticky-logo',
	          'property' => 'width',
	          'units'    => 'px',
	        ),
	      ),
	    );

	  $fields[] = array(
	    'type'        => 'image',
	    'settings'    => 'sticky_logo_image',
	    'label'       => __( 'Sticky Logo', 'whisk' ),
	    'description' => __( 'Upload an image. Make sure you set the width to half the size of the original logo.', 'whisk' ),
	    'section'     => 'header_options',
	    'default'     => '',
	    'priority'    => 10,
	  );


	  // Header Text Logo Typography
	  $fields[] = array(
	    'type'        => 'typography',
	    'settings'    => 'header_branding_typography',
	    'label'       => esc_attr__( 'Header Text Branding Typography', 'whisk' ),
	    'section'     => 'header_options',
	    'default'     => array(
	      'font-family'    => 'Open Sans',
	      'variant'        => 'regular',
	      'font-size'      => '18px',
	      'letter-spacing' => '0',
	      'subsets'        => array( 'latin-ext' ),
	      'color'          => '#333333',
	      'text-transform' => 'none',
	      'text-align'     => 'left'
	    ),
	    'priority'    => 10,
	    'output'      => array(
	      array(
	        'element' => '.navbar-default a.navbar-brand, .navbar-default a.navbar-brand:hover, .navbar-default a.navbar-brand:focus',
	      ),
	    ),
	  );


// Logo/Branding Padding
/* $fields[] = array(
	'type'        => 'number',
	'settings'    => 'logo_width',
	'label'       => __( 'Logo Width', 'whisk' ),
	'description' => __( 'Set the width of your logo/brand image.', 'whisk' ),
	'section'     => 'header_options',
	'priority'    => 10,
	'default'     => '150',
	'choices'     => array(
		'min'  => '0',
		'max'  => '300',
		'step' => '1',
	),
	'output'      => array(
		array(
			'element' => '.sticky-logo, .desktop-logo',
			'property' => 'max-width',
			'units'    => 'px',
		),
	),
); */

  // Logo/Branding Padding
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'logo_padding',
    'label'       => __( 'Logo Top Padding', 'whisk' ),
    'description' => __( 'If you are not using a logo image and are using only text set the top padding to 0px', 'whisk' ),
    'section'     => 'header_options',
    'priority'    => 10,
    'default'     => '0',
    'choices'     => array(
  		'min'  => '0',
  		'max'  => '300',
  		'step' => '1',
  	),
    'output'      => array(
      array(
        'element' => '.desktop a.navbar-brand .desktop-logo',
        'property' => 'padding-top',
        'units'    => 'px',
      ),
    ),
  );


  // Logo/Branding Padding
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'sticky_logo_padding',
    'label'       => __( 'Sticky Logo Top Padding', 'whisk' ),
    'description' => __( 'If you are not using a logo image and are using only text set the top padding to 0px', 'whisk' ),
    'section'     => 'header_options',
    'priority'    => 10,
    'default'     => '0',
    'choices'     => array(
      'min'  => '0',
      'max'  => '300',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => '.desktop .sticky a.navbar-brand .sticky-logo',
        'property' => 'padding-top',
        'units'    => 'px',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'sticky_header',
        'operator' => '==',
        'value' => 'navbar-fixed-top'
      ),
    ),
  );


  // Header Background Color
  $fields[] = array(
    'type'        => 'color',
  	'settings'    => 'header_background_color',
  	'label'       => __( 'Header Background Color', 'whisk' ),
  	'section'     => 'header_options',
  	'default'     => '#F8F8F8',
  	'priority'    => 10,
  	'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.desktop .navbar-default',
        'property' => 'background-color',
      ),
    ),
  );

  // Sticky Header Background Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'sticky_header_background_color',
    'label'       => __( 'Sticky Header Background Color', 'whisk' ),
    'section'     => 'header_options',
    'default'     => '#F8F8F8',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.navbar-default.sticky',
        'property' => 'background-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'sticky_header',
        'operator' => '==',
        'value' => 'navbar-fixed-top'
      ),
    ),
  );

  // Header Background Image
  $fields[] = array(
      'type'        => 'background',
      'settings'    => 'header_background_image',
      'label'       => __( 'Header Background Image', 'whisk' ),
      'section'     => 'header_options',
      'default'     => array(
          'image'    => '',
          'repeat'   => 'no-repeat',
          'size'     => 'cover',
          'attach'   => 'initial',
          'position' => 'left-top',
      ),
      'priority'    => 10,
      'output'      => array(
        array(
          'element' => '.navbar-default',
        ),
      ),
  );

  // Header Elements Height
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'header_height',
    'label'       => esc_attr__( 'Header Elements Height', 'whisk' ),
    'description' => esc_attr__( 'Adjust the height of the menu and branding/logo sections of the header.', 'whisk' ),
    'section'     => 'header_options',
    'default'     => 90,
    'choices'     => array(
      'min'  => '50',
      'max'  => '300',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => 'a.navbar-brand, .navbar-nav li a, .navbar-nav li a',
        'property' => 'line-height',
  			'units'    => 'px',
      ),
      array(
        'element' => '.desktop .navbar-default, .whisk-navbar-left .navbar-brand, .whisk-navbar-left .navbar-nav li a',
        'property' => 'height',
  			'units'    => 'px',
      ),
    ),
  );

  // Sticky Header Elements Height
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'sticky_header_height',
    'label'       => esc_attr__( 'Sticky Header Elements Height', 'whisk' ),
    'description' => esc_attr__( 'Adjust the sticky height of the menu and branding/logo sections of the header. ', 'whisk' ),
    'section'     => 'header_options',
    'default'     => 70,
    'choices'     => array(
      'min'  => '50',
      'max'  => '300',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => '.desktop .sticky .navbar-nav li a',
        'property' => 'line-height',
        'units'    => 'px',
      ),
      array(
        'element' => '.navbar-default.sticky, .sticky .whisk-navbar-left a.navbar-brand, .sticky .whisk-navbar-left a.navbar-brand, .sticky .whisk-navbar-left .navbar-nav li a',
        'property' => 'height',
        'units'    => 'px',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'sticky_header',
        'operator' => '==',
        'value' => 'navbar-fixed-top'
      ),
    ),
  );

  // Toggle Sticky Header
  $fields[] = array(
    'type'        => 'radio-buttonset',
  	'settings'    => 'header_border_toggle',
  	'label'       => __( 'Toggle Header Bottom Border', 'whisk' ),
  	'section'     => 'header_options',
  	'default'     => 'no-border',
  	'priority'    => 10,
  	'choices'     => array(
  		'no-border'   => esc_attr__( 'Off', 'whisk' ),
  		'border' => esc_attr__( 'On', 'whisk' ),
  	),
  );

  // Header Bottom Border Height
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'header_bottom_border_height',
    'label'       => esc_attr__( 'Header Bottom Border Height', 'whisk' ),
    'section'     => 'header_options',
    'default'     => 4,
    'choices'     => array(
      'min'  => '0',
      'max'  => '300',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element'       => '.navbar-fixed-top',
        'property'      => 'border-width',
        'units'         => 'px',
        'value_pattern' => '0 0 $',
      ),
      array(
        'element'       => '.navbar-static-top',
        'property'      => 'border-width',
        'units'         => 'px',
        'value_pattern' => '0 0 $',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'header_border_toggle',
        'operator' => '==',
        'value' => 'border'
      ),
    ),
  );

  // Header Border Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'header_border_color',
    'label'       => __( 'Header Border Color', 'whisk' ),
    'section'     => 'header_options',
    'default'     => '#e7e7e7',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.navbar-default',
        'property' => 'border-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'header_border_toggle',
        'operator' => '==',
        'value' => 'border'
      ),
    ),
  );

  // Logo Position
  $fields[] = array(
    'type'        => 'radio-buttonset',
    'settings'    => 'logo_position',
    'label'       => __( 'Brand/Logo Postion', 'whisk' ),
    'section'     => 'header_options',
    'default'     => 'whisk-navbar-left',
    'priority'    => 10,
    'choices'     => array(
      'whisk-navbar-left'   => esc_attr__( 'Left', 'whisk' ),
      'branding-center'  => esc_attr__( 'Center', 'whisk' ),
      'whisk-navbar-right' => esc_attr__( 'Right', 'whisk' ),
    ),
  );

  // Toggle Pre Header
  $fields[] = array(
    'type'        => 'radio-buttonset',
  	'settings'    => 'pre_header_toggle',
  	'label'       => __( 'Toggle Pre Header', 'whisk' ),
  	'section'     => 'pre_header',
  	'default'     => 'hide-pre-header',
  	'priority'    => 10,
  	'choices'     => array(
  		'hide-pre-header'   => esc_attr__( 'Off', 'whisk' ),
  		'show-pre-header' => esc_attr__( 'On', 'whisk' ),
  	),
  );

  // Pre Header Grid Layout
  $fields[] = array(
    'label'       => __( 'Pre Header Grid Layout', 'whisk' ),
    'section'     => 'pre_header',
    'settings'    => 'pre_header_grid',
    'type'        => 'select',
    'priority'    => 10,
    'choices'     => array(
        'option-1' => esc_attr__( '1 Column', '1' ),
        'option-2' => esc_attr__( '2 Column', '2' ),
        'option-3' => esc_attr__( '3 Column', '3' ),
        'option-4' => esc_attr__( '4 Column', '4' ),
        'option-6' => esc_attr__( '6 Column', '6' ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'pre_header_toggle',
        'operator' => '==',
        'value' => 'show-pre-header'
      ),
    ),
  );

  // Pre Header Background Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'pre_header_color',
    'label'       => __( 'Pre Header Background Color', 'whisk' ),
    'section'     => 'pre_header',
    'default'     => '#3d4045',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.pre-header',
        'property' => 'background-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'pre_header_toggle',
        'operator' => '==',
        'value' => 'show-pre-header'
      ),
    ),
  );

  // Toggle Post Header
  $fields[] = array(
    'type'        => 'radio-buttonset',
  	'settings'    => 'post_header_toggle',
  	'label'       => __( 'Toggle Post Header', 'whisk' ),
  	'section'     => 'post_header',
  	'default'     => 'hide-post-header',
  	'priority'    => 10,
  	'choices'     => array(
  		'hide-post-header'   => esc_attr__( 'Off', 'whisk' ),
  		'show-post-header' => esc_attr__( 'On', 'whisk' ),
  	),
  );

  // Post Header Grid Layout
  $fields[] = array(
    'section'     => 'post_header',
    'label'       => __( 'Post Header Grid Layout', 'whisk' ),
    'settings'    => 'post_header_grid',
    'type'        => 'select',
    'priority'    => 10,
    'choices'     => array(
        'option-1' => esc_attr__( '1 Column', '1' ),
        'option-2' => esc_attr__( '2 Column', '2' ),
        'option-3' => esc_attr__( '3 Column', '3' ),
        'option-4' => esc_attr__( '4 Column', '4' ),
        'option-6' => esc_attr__( '6 Column', '6' ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'post_header_toggle',
        'operator' => '==',
        'value' => 'show-post-header'
      ),
    ),
  );

  // Post Header Background Color
  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'post_header_color',
    'label'       => __( 'Post Header Background Color', 'whisk' ),
    'section'     => 'post_header',
    'default'     => '#3d4045',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.post-header',
        'property' => 'background-color',
      ),
    ),
    'active_callback' => array(
      array(
        'setting' => 'post_header_toggle',
        'operator' => '==',
        'value' => 'show-post-header'
      ),
    ),
  );

	// Menu Link Typography
	$fields[] = array(
		'type'        => 'typography',
		'settings'    => 'header_menu_typography',
		'label'       => esc_attr__( 'Menu Typography', 'whisk' ),
		'section'     => 'header_options',
		'default'     => array(
			'font-family'    => 'Open Sans',
			'variant'        => 'regular',
			'font-size'      => '14px',
			'letter-spacing' => '0',
			'subsets'        => array( 'latin-ext' ),
			'color'          => '#333333',
			'text-transform' => 'none',
			'text-align'     => 'left',
			'line-height'    => 'auto',
		),
		'priority'    => 10,
		'output'      => array(
			array(
				'element' => 'header.navbar-default .navbar-nav > li > a',
			),
		),
	);

	$fields[] = array(
		'type'        => 'typography',
		'settings'    => 'sticky_header_menu_typography',
		'label'       => esc_attr__( 'Sticky Menu Typography', 'whisk' ),
		'section'     => 'header_options',
		'default'     => array(
			'font-family'    => 'Open Sans',
			'variant'        => 'regular',
			'font-size'      => '14px',
			'letter-spacing' => '0',
			'subsets'        => array( 'latin-ext' ),
			'color'          => '#333333',
			'text-transform' => 'none',
			'text-align'     => 'left',
			'line-height'    => 'auto',
		),
		'priority'    => 10,
		'output'      => array(
			array(
				'element' => 'header.navbar-default.sticky .navbar-nav > li > a',
			),
		),
		'active_callback' => array(
			array(
				'setting' => 'sticky_header',
				'operator' => '==',
				'value' => 'navbar-fixed-top'
			),
		),
	);

	// Menu Item Text Hover Styles
	$fields[] = array(
		'type'        => 'typography',
		'settings'    => 'header_menu_hover',
		'label'       => esc_attr__( 'Menu Item Text Hover Color', 'whisk' ),
		'section'     => 'header_options',
		'default'     => array(
			'font-family'    => 'Open Sans',
			'variant'        => 'regular',
			'font-size'      => '14px',
			'letter-spacing' => '0',
			'subsets'        => array( 'latin-ext' ),
			'color'          => '#333333',
			'text-transform' => 'none',
			'text-align'     => 'left',
			'line-height'    => 'auto',
		),
		'priority'    => 10,
		'output'      => array(
			array(
				'element' => 'header.navbar-default .navbar-nav > li > a:hover',
			),
		),
	);

	// Menu Item Background Hover Color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'header_menu_hover_background',
		'label'       => esc_attr__( 'Menu Item Background Hover Color', 'whisk' ),
		'section'     => 'header_options',
		'default'     => 'rgba(0,0,0,0)',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.desktop header.navbar-default .navbar-nav > li > a:hover',
				'property' => 'background-color',
			),
		),
	);

	// Active Menu Item Text Styles
	$fields[] = array(
		'type'        => 'typography',
		'settings'    => 'header_menu_active',
		'label'       => esc_attr__( 'Active Menu Item Text Color', 'whisk' ),
		'section'     => 'header_options',
		'default'     => array(
			'font-family'    => 'Open Sans',
			'variant'        => 'regular',
			'font-size'      => '14px',
			'letter-spacing' => '0',
			'subsets'        => array( 'latin-ext' ),
			'color'          => '#333333',
			'text-transform' => 'none',
			'text-align'     => 'left',
			'line-height'    => 'auto',
		),
		'priority'    => 10,
		'output'      => array(
			array(
				'element' => '.desktop header.navbar-default .navbar-nav > .active > a, .desktop .navbar-default .navbar-nav > .active > a:focus, .desktop .navbar-default .navbar-nav>li>a:focus',
			),
		),
	);

	// Active Menu Item Background Color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'header_menu_active_background',
		'label'       => esc_attr__( 'Active Menu Item Background Color', 'whisk' ),
		'section'     => 'header_options',
		'default'     => '#E7E7E7',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.desktop header.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .desktop .navbar-default .navbar-nav > .active > a:hover',
				'property' => 'background-color',
			),
		),
	);

	$fields[] = array(
		'type'        => 'typography',
		'settings'    => 'dropdown_menu_typography',
		'label'       => esc_attr__( 'Dropdown Menu Typography', 'whisk' ),
		'section'     => 'header_options',
		'default'     => array(
			'font-family'    => 'Open Sans',
			'variant'        => 'regular',
			'font-size'      => '14px',
			'letter-spacing' => '0',
			'subsets'        => array( 'latin-ext' ),
			'color'          => '#333333',
			'text-transform' => 'none',
			'text-align'     => 'left',
			'line-height'    => '5',
		),
		'priority'    => 10,
		'output'      => array(
			array(
				'element' => '.desktop .dropdown-menu li a, .desktop .sticky .dropdown-menu li a',
			),
		),
	);

	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'dropdown_menu_line_height',
		'label'       => __( 'Dropdown Menu Li Height - must correspond with line-height set for dropdown, Font Size x Line Height = Li Height', 'whisk' ),
		'description' => __( 'Adjust the line height', 'whisk' ),
		'section'     => 'header_options',
		'priority'    => 10,
		'default'     => '70',
		'choices'     => array(
			'min'  => '0',
			'max'  => '500',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element' => '.desktop .dropdown-menu li a, .desktop .sticky .dropdown-menu li a',
				'property' => 'height',
				'units'    => 'px',
			),
		),
	);

	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'dropdown_menu_padding_left',
		'label'       => __( 'Dropdown menu link padding left', 'whisk' ),
		'description' => __( 'Adjust the left padding', 'whisk' ),
		'section'     => 'header_options',
		'priority'    => 10,
		'default'     => '15',
		'choices'     => array(
			'min'  => '0',
			'max'  => '500',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element' => '.desktop .dropdown-menu li a',
				'property' => 'padding-left',
				'units'    => 'px',
			),
		),
	);


	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'dropdown_menu_padding_right',
		'label'       => __( 'Dropdown menu link padding right', 'whisk' ),
		'description' => __( 'Adjust the right padding', 'whisk' ),
		'section'     => 'header_options',
		'priority'    => 10,
		'default'     => '15',
		'choices'     => array(
			'min'  => '0',
			'max'  => '500',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element' => '.desktop .dropdown-menu li a',
				'property' => 'padding-right',
				'units'    => 'px',
			),
		),
	);


	// Dropdown Menu Background Color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'dropdown_menu_background',
		'label'       => esc_attr__( 'Dropdown Menu Background Color', 'whisk' ),
		'section'     => 'header_options',
		'default'     => '#E7E7E7',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.desktop .dropdown li',
				'property' => 'background-color',
			),
		),
	);

	// Dropdown Menu Item Hover Background Color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'dropdown_hover_background',
		'label'       => esc_attr__( 'Dropdown Menu A Hover Background Color', 'whisk' ),
		'section'     => 'header_options',
		'default'     => '#E7E7E7',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.desktop .dropdown li a:hover',
				'property' => 'background-color',
			),
		),
	);

	$fields[] = array(
    'type'        => 'radio-buttonset',
  	'settings'    => 'mobile_sticky_header',
  	'label'       => __( 'Toggle Mobile Sticky Header', 'whisk' ),
  	'section'     => 'header_options',
  	'default'     => 'navbar-static-top',
  	'priority'    => 10,
  	'choices'     => array(
  		'navbar-static-top'   => esc_attr__( 'Off', 'whisk' ),
  		'navbar-fixed-top' => esc_attr__( 'On', 'whisk' ),
  	),
  );

	$fields[] = array(
    'type'        => 'number',
    'settings'    => 'mobile_header_height',
    'label'       => __( 'Mobile Header Min Height', 'whisk' ),
    'description' => __( 'Adjust the Header min height', 'whisk' ),
    'section'     => 'header_options',
    'priority'    => 10,
    'default'     => '60',
    'choices'     => array(
      'min'  => '0',
      'max'  => '500',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => '.mobile .navbar-header',
        'property' => 'min-height',
        'units'    => 'px',
      ),
    ),
  );



$fields[] = array(
	'type'        => 'number',
	'settings'    => 'mobile_logo_width',
	'label'       => __( 'Mobile Logo Width', 'whisk' ),
	'description' => __( 'Set the width to half the size of your logo', 'whisk' ),
	'section'     => 'header_options',
	'priority'    => 10,
	'default'     => '150',
	'choices'     => array(
		'min'  => '0',
		'max'  => '500',
		'step' => '1',
	),
	'output'      => array(
		array(
			'element' => '.mobile a.navbar-brand .mobile-logo',
			'property' => 'width',
			'units'    => 'px',
		),
	),
);

$fields[] = array(
	'type'        => 'image',
	'settings'    => 'mobile_logo_image',
	'label'       => __( 'Mobile Logo', 'whisk' ),
	'description' => __( 'Upload an image. Make sure you set the width to half the size of the original logo.', 'whisk' ),
	'section'     => 'header_options',
	'default'     => '',
	'priority'    => 10,
);

$fields[] = array(
	'type'        => 'number',
	'settings'    => 'mobile_logo_margin',
	'label'       => __( 'Mobile Logo Top Margin', 'whisk' ),
	'description' => __( 'Set the width to half the size of your logo', 'whisk' ),
	'section'     => 'header_options',
	'priority'    => 10,
	'default'     => '5',
	'choices'     => array(
		'min'  => '0',
		'max'  => '500',
		'step' => '1',
	),
	'output'      => array(
		array(
			'element' => '.mobile a.navbar-brand .mobile-logo',
			'property' => 'margin-top',
			'units'    => 'px',
		),
	),
);


//mobile header options
  $fields[] = array(
    'type'        => 'typography',
    'settings'    => 'mobile_dropdown_menu_typography',
    'label'       => esc_attr__( 'Mobile dropdown Menu Typography', 'whisk' ),
    'section'     => 'header_options',
    'default'     => array(
      'font-family'    => 'Open Sans',
      'variant'        => 'regular',
      'font-size'      => '14px',
      'letter-spacing' => '0',
      'subsets'        => array( 'latin-ext' ),
      'color'          => '#333333',
      'text-transform' => 'none',
      'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
      array(
        'element' => '.mobile .nav.navbar-nav li a',
      ),
    ),
  );

  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'mobile_dropdown_menu_line_height',
    'label'       => __( 'Mobile dropdown Menu Line Height', 'whisk' ),
    'description' => __( 'Adjust the line height', 'whisk' ),
    'section'     => 'header_options',
    'priority'    => 10,
    'default'     => '50',
    'choices'     => array(
      'min'  => '0',
      'max'  => '500',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => '.mobile .nav.navbar-nav li a',
        'property' => 'line-height',
        'units'    => 'px',
      ),
    ),
  );

  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'mobile_dropdown_menu_padding_left',
    'label'       => __( 'Mobile dropdown padding left', 'whisk' ),
    'description' => __( 'Adjust the left padding', 'whisk' ),
    'section'     => 'header_options',
    'priority'    => 10,
    'default'     => '15',
    'choices'     => array(
      'min'  => '0',
      'max'  => '500',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => '.mobile .nav.navbar-nav li a',
        'property' => 'padding-left',
        'units'    => 'px',
      ),
    ),
  );


  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'mobile_dropdown_menu_padding_right',
    'label'       => __( 'Mobile dropdown Menu padding right', 'whisk' ),
    'description' => __( 'Adjust the right padding', 'whisk' ),
    'section'     => 'header_options',
    'priority'    => 10,
    'default'     => '15',
    'choices'     => array(
      'min'  => '0',
      'max'  => '500',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element' => '.mobile .nav.navbar-nav li a',
        'property' => 'padding-right',
        'units'    => 'px',
      ),
    ),
  );


  $fields[] = array(
    'type'        => 'color',
    'settings'    => 'mobile_header_background_color',
    'label'       => __( 'Mobile Header Background Color', 'whisk' ),
    'section'     => 'header_options',
    'default'     => '#F8F8F8',
    'priority'    => 10,
    'alpha'       => true,
    'output'      => array(
      array(
        'element' => '.mobile .navbar-header',
        'property' => 'background-color',
      ),
    ),
  );

  //mobile header options
    $fields[] = array(
      'type'        => 'color',
      'settings'    => 'mobile_nav_background_color',
      'label'       => __( 'Mobile Nav Background Color', 'whisk' ),
      'section'     => 'header_options',
      'default'     => '#F8F8F8',
      'priority'    => 10,
      'alpha'       => true,
      'output'      => array(
        array(
          'element' => '.mobile .navbar-collapse.collapse, .mobile .navbar-collapse.collapse.in, .mobile .navbar-collapse.collapse ul, .mobile .navbar-default',
          'property' => 'background-color',
        ),
      ),
    );

    //mobile header options
      $fields[] = array(
        'type'        => 'color',
        'settings'    => 'mobile_nav_link_color',
        'label'       => __( 'Mobile Nav Link Color', 'whisk' ),
        'section'     => 'header_options',
        'default'     => '#F8F8F8',
        'priority'    => 10,
        'alpha'       => true,
        'output'      => array(
          array(
            'element' => '.mobile header.navbar-default .navbar-nav > li > a',
            'property' => 'color',
          ),
        ),
      );

			$fields[] = array(
				'type'        => 'number',
				'settings'    => 'mobile_hamburger_margin',
				'label'       => __( 'Hamburger Menu Top Margin', 'whisk' ),
				'description' => __( 'Set the width to half the size of your logo', 'whisk' ),
				'section'     => 'header_options',
				'priority'    => 10,
				'default'     => '8',
				'choices'     => array(
					'min'  => '0',
					'max'  => '500',
					'step' => '1',
				),
				'output'      => array(
					array(
						'element' => '.mobile .navbar-toggle',
						'property' => 'margin-top',
						'units'    => 'px',
					),
				),
			);
      //mobile header options
        $fields[] = array(
          'type'        => 'color',
          'settings'    => 'mobile_hamburger_button_background_color',
          'label'       => __( 'Mobile Hamburger Button Background Color', 'whisk' ),
          'section'     => 'header_options',
          'default'     => '#F8F8F8',
          'priority'    => 10,
          'alpha'       => true,
          'output'      => array(
            array(
              'element' => '.mobile .navbar-default .navbar-toggle:focus, .mobile .navbar-default .navbar-toggle:hover',
              'property' => 'background-color',
            ),
          ),
        );

        //mobile header options
          $fields[] = array(
            'type'        => 'color',
            'settings'    => 'mobile_hamburger_border_color',
            'label'       => __( 'Mobile Hamburger Border Color', 'whisk' ),
            'section'     => 'header_options',
            'default'     => '#F8F8F8',
            'priority'    => 10,
            'alpha'       => true,
            'output'      => array(
              array(
                'element' => '.mobile .navbar-default .navbar-toggle',
                'property' => 'border-color',
              ),
            ),
          );

          //mobile header options
            $fields[] = array(
              'type'        => 'color',
              'settings'    => 'mobile_hamburger_lines_background_color',
              'label'       => __( 'Mobile Hamburger Lines Background Color', 'whisk' ),
              'section'     => 'header_options',
              'default'     => '#F8F8F8',
              'priority'    => 10,
              'alpha'       => true,
              'output'      => array(
                array(
                  'element' => '.mobile .navbar-default .navbar-toggle .icon-bar',
                  'property' => 'background-color',
                ),
              ),
            );



  return $fields;
}
add_filter( 'kirki/fields', 'whisk_header_fields' );
