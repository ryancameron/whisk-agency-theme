<?php
/**
 * Add Kirki panels and section that you would like to appear in the customizer
 */

function whisk_panels_sections( $wp_customize ) {
  /**
  * Add Header Panel
  */

  whisk_kirki::add_panel( 'header_options_panel', array(
  'priority'    => 1,
  'title'       => __( 'Header', 'whisk' ),
  'description' => __( 'Website header options', 'whisk' ),
  ) );

	// Pre Header
	whisk_kirki::add_section( 'pre_header', array(
	 'title'       => __( 'Pre Header', 'whisk' ),
	 'priority'    => 10,
	 'panel'       => 'header_options_panel',
	 'description' => __( 'Area above header', 'whisk' ),
	) );

  // Header Options
  whisk_kirki::add_section( 'header_options', array(
   'title'       => __( 'Header Options', 'whisk' ),
   'priority'    => 10,
   'panel'       => 'header_options_panel',
   'description' => __( 'Header colors and dimensions', 'whisk' ),
  ) );

  // Pre Header
  whisk_kirki::add_section( 'post_header', array(
   'title'       => __( 'Post Header', 'whisk' ),
   'priority'    => 10,
   'panel'       => 'header_options_panel',
   'description' => __( 'Area below header', 'whisk' ),
  ) );

  // Header Menu Options
  whisk_kirki::add_section( 'header_menu_options', array(
   'title'       => __( 'Header Menu Options', 'whisk' ),
   'priority'    => 10,
   'panel'       => 'header_options_panel',
   'description' => __( 'Main menu options', 'whisk' ),
  ) );



	/**
	* Add Look & Feel Panel
	*/

	whisk_kirki::add_panel( 'lookfeel_options_panel', array(
	'priority'    => 2,
	'title'       => __( 'Look & Feel', 'whisk' ),
	'description' => __( 'Look & Feel options', 'whisk' ),
	) );

	// Layout
	whisk_kirki::add_section( 'layout', array(
	 'title'       => __( 'Layout', 'whisk' ),
	 'priority'    => 10,
	 'panel'       => 'lookfeel_options_panel',
	 'description' => __( 'Layout', 'whisk' ),
	) );

	// Typography
	whisk_kirki::add_section( 'typography', array(
	 'title'       => __( 'Typography', 'whisk' ),
	 'priority'    => 10,
	 'panel'       => 'lookfeel_options_panel',
	 'description' => __( 'Typography', 'whisk' ),
	) );

	// Buttons, Modals, Labels
	whisk_kirki::add_section( 'buttonsetc', array(
	 'title'       => __( 'Buttons, Alerts, Labels', 'whisk' ),
	 'priority'    => 10,
	 'panel'       => 'lookfeel_options_panel',
	 'description' => __( 'Buttons, Alerts, Labels', 'whisk' ),
	) );

	// Page Header
	whisk_kirki::add_section( 'pageheader', array(
	 'title'       => __( 'Page Header Options', 'whisk' ),
	 'priority'    => 10,
	 'panel'       => 'lookfeel_options_panel',
	 'description' => __( 'Page Header Options', 'whisk' ),
	) );



	/**
	* Advanced Settings Panel
	*/

	whisk_kirki::add_panel( 'advanced_options_panel', array(
		'priority'    => 300,
		'title'       => __( 'Advanced Options', 'whisk' ),
		'description' => __( 'Advanced Options', 'whisk' ),
	) );

	// WooCommerce section
	whisk_kirki::add_section( 'woocommerce', array(
	 'title'       => __( 'WooCommerce', 'whisk' ),
	 'priority'    => 10,
	 'panel'       => 'advanced_options_panel',
	 'description' => __( 'WooCommerce', 'whisk' ),
	) );


  /**
  * Footer Panel
  */

  whisk_kirki::add_panel( 'footer_options_panel', array(
  'priority'    => 3,
  'title'       => __( 'Footer', 'whisk' ),
  'description' => __( 'Website footer options', 'whisk' ),
  ) );

  // Footer Options
  whisk_kirki::add_section( 'footer_options', array(
    'priority'    => 10,
    'title'       => __( 'Footer Options', 'whisk' ),
    'panel'       => 'footer_options_panel',
    'description' => __( 'Footer options', 'whisk' ),
  ) );

  // Pre Footer
  whisk_kirki::add_section( 'pre_footer', array(
   'title'       => __( 'Pre Footer', 'whisk' ),
   'priority'    => 10,
   'panel'       => 'footer_options_panel',
   'description' => __( 'Area above footer', 'whisk' ),
  ) );

  // Pre Footer
  whisk_kirki::add_section( 'post_footer', array(
   'title'       => __( 'Post Footer', 'whisk' ),
   'priority'    => 10,
   'panel'       => 'footer_options_panel',
   'description' => __( 'Area below footer', 'whisk' ),
  ) );

  // Footer Menu Options
  whisk_kirki::add_section( 'footer_menu_options', array(
   'title'       => __( 'Footer Menu Options', 'whisk' ),
   'priority'    => 10,
   'panel'       => 'footer_options_panel',
   'description' => __( 'Footer menu options', 'whisk' ),
  ) );


}

add_action( 'customize_register', 'whisk_panels_sections' );
