<?
function whisk_look_feel_fields( $fields ) {

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'body',
  	'label'       => esc_attr__( 'Body styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
  		'font-family'    => 'Open Sans',
  		'variant'        => 'regular',
  		'font-size'      => '14px',
  		'line-height'    => '1.5',
  		'letter-spacing' => '0',
  		'subsets'        => array( 'latin-ext' ),
  		'color'          => '#333333',
  		'text-transform' => 'inherit',
  		'text-align'     => 'left'
  	),
  	'priority'    => 10,
  	'output'      => array(
  		array(
  			'element' => 'body',
  		),
  	),
  );

  $fields[] = array(
    'type'        => 'typography',
    'settings'    => 'h1',
    'label'       => esc_attr__( 'H1 styles', 'whisk' ),
		'section'     => 'typography',
		'default'     => array(
  		'font-family'    => 'Open Sans',
  	  'variant'        => 'regular',
  	  'font-size'      => '36px',
  	  'line-height'    => '1.5',
  	  'letter-spacing' => '0',
  	  'subsets'        => array( 'latin-ext' ),
  	  'color'          => '#333333',
  	  'text-transform' => 'inherit',
  	  'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
  	  array(
  		  'element' => 'h1',
  	  ),
    ),
  );

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'h2',
  	'label'       => esc_attr__( 'H2 styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
    	'font-family'    => 'Open Sans',
    	'variant'        => 'regular',
    	'font-size'      => '30px',
    	'line-height'    => '1.5',
    	'letter-spacing' => '0',
    	'subsets'        => array( 'latin-ext' ),
    	'color'          => '#333333',
    	'text-transform' => 'inherit',
    	'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
    	array(
    		'element' => 'h2',
    	),
    ),
  );

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'h3',
  	'label'       => esc_attr__( 'H3 styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
    	'font-family'    => 'Open Sans',
    	'variant'        => 'regular',
    	'font-size'      => '24px',
    	'line-height'    => '1.5',
    	'letter-spacing' => '0',
    	'subsets'        => array( 'latin-ext' ),
    	'color'          => '#333333',
    	'text-transform' => 'inherit',
    	'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
    	array(
    		'element' => 'h3',
    	),
    ),
  );

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'h4',
  	'label'       => esc_attr__( 'H4 Styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
    	'font-family'    => 'Open Sans',
    	'variant'        => 'regular',
    	'font-size'      => '18px',
    	'line-height'    => '1.5',
    	'letter-spacing' => '0',
    	'subsets'        => array( 'latin-ext' ),
    	'color'          => '#333333',
    	'text-transform' => 'inherit',
    	'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
    	array(
    		'element' => 'h4',
    	),
    ),
  );

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'h5',
  	'label'       => esc_attr__( 'H5 Styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
    	'font-family'    => 'Open Sans',
    	'variant'        => 'regular',
    	'font-size'      => '14px',
    	'line-height'    => '1.5',
    	'letter-spacing' => '0',
    	'subsets'        => array( 'latin-ext' ),
    	'color'          => '#333333',
    	'text-transform' => 'inherit',
    	'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
    	array(
    		'element' => 'h5',
    	),
    ),
  );

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'h6',
  	'label'       => esc_attr__( 'H6 Styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
    	'font-family'    => 'Open Sans',
    	'variant'        => 'regular',
    	'font-size'      => '12px',
    	'line-height'    => '1.5',
    	'letter-spacing' => '0',
    	'subsets'        => array( 'latin-ext' ),
    	'color'          => '#333333',
    	'text-transform' => 'inherit',
    	'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
    	array(
    		'element' => 'h6',
    	),
    ),
  );

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'links',
  	'label'       => esc_attr__( 'Link Styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
    	'font-family'    => 'inherit',
    	'variant'        => 'inherit',
    	'font-size'      => 'inherit',
    	'line-height'    => 'inherit',
    	'letter-spacing' => '0',
    	'subsets'        => array( 'latin-ext' ),
    	'color'          => '#333333',
    	'text-transform' => 'inherit',
    	'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
    	array(
    		'element' => 'a',
    	),
    ),
  );

  $fields[] = array(
  	'type'        => 'typography',
  	'settings'    => 'links_hover',
  	'label'       => esc_attr__( 'Link Hover Styles', 'whisk' ),
  	'section'     => 'typography',
  	'default'     => array(
    	'font-family'    => 'inherit',
    	'variant'        => 'inherit',
    	'font-size'      => 'inherit',
    	'line-height'    => 'inherit',
    	'letter-spacing' => '0',
    	'subsets'        => array( 'latin-ext' ),
    	'color'          => '#111111',
    	'text-transform' => 'inherit',
    	'text-align'     => 'left'
    ),
    'priority'    => 10,
    'output'      => array(
    	array(
    		'element' => 'a:hover, a:focus',
    	),
    ),
  );

	/*
	* Layout
	*/



	// Add a Field to change the body background color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'body_background_color',
		'label'       => __( 'Body background color.', 'whisk' ),
		'description' => __( 'The Body background color specify here will apply to the body area.', 'whisk' ),
		'section'     => 'layout',
		'default'     => '#fff',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => 'body',
				'property' => 'background-color',
			),
		),
	);

	// Add a Field to change the body background image
	$fields[] = array(
			'type'        => 'background',
			'settings'    => 'body_background_image',
			'label'       => __( 'Body background image', 'whisk' ),
			'description' => __( 'The Body background you specify here will apply to the body area.', 'whisk' ),
			'section'     => 'layout',
			'default'     => array(
					'image'    => '',
					'repeat'   => 'no-repeat',
					'size'     => 'cover',
					'attach'   => 'fixed',
					'position' => 'left-top',
			),
			'priority'    => 10,
			'output'      => array(
				array(
					'element' => 'body',
				),
			),
	);

	// Add a Field to change the content background color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'content_background_color',
		'label'       => __( 'Content background color.', 'whisk' ),
		'description' => __( 'The Content background color specify here will apply to the content area.', 'whisk' ),
		'section'     => 'layout',
		'default'     => '#fff',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.wrap',
				'property' => 'background-color',
			),
		),
	);

	// Add a Field to change the content background image
	$fields[] = array(
			'type'        => 'background',
			'settings'    => 'content_background_image',
			'label'       => __( 'Content background image', 'whisk' ),
			'description' => __( 'The content background you specify here will apply to the body area.', 'whisk' ),
			'section'     => 'layout',
			'default'     => array(
					'image'    => '',
					'repeat'   => 'no-repeat',
					'size'     => 'cover',
					'attach'   => 'fixed',
					'position' => 'left-top',
			),
			'priority'    => 10,
			'output'      => array(
				array(
					'element' => '.wrap',
				),
			),
	);


/*	// Add a Field to change the Sidebar Widget style
	$fields[] = array(
		'type'        => 'radio-buttonset',
		'settings'    => 'w_setting',
		'label'       => __( 'Sidebar widget style', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'option-1',
		'priority'    => 10,
		'choices'     => array(
			'option-1'   => esc_attr__( 'None', 'whisk' ),
			'option-2' => esc_attr__( 'Panel', 'whisk' ),
			'option-3'  => esc_attr__( 'Well', 'whisk' ),
		),
	);

	// Add a Field to change the Sidebar Panel Colors
	$fields[] = array(
		'type'        => 'select',
		'settings'    => 'w_setting_color',
		'label'       => __( 'Sidebar widget panel colors', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'default',
		'priority'    => 10,
		'choices'     => array(
			'default'   => esc_attr__( 'Default', 'whisk' ),
			'primary' => esc_attr__( 'Primary', 'whisk' ),
			'success'  => esc_attr__( 'Success', 'whisk' ),
			'warning'  => esc_attr__( 'Warning', 'whisk' ),
			'danger'  => esc_attr__( 'Danger', 'whisk' ),
		),
		'active_callback'  => array(
		array(
			'setting'  => 'w_setting',
			'operator' => '==',
			'value'    => 'option-2',
		),
	)
	);

	// Add a Field to change the Sidebar Well Border-radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'well_radius',
		'label'       => __( 'Well Radius ', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'default',
		'priority'    => 10,
		'default'     => 6,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.sidebar .well',
				'property'      => 'border-radius',
				'units'         => 'px',
			),
		),
		'active_callback'  => array(
		array(
			'setting'  => 'w_setting',
			'operator' => '==',
			'value'    => 'option-3',
		),
	)
	);

	// Add a Field to change the Sidebar Panel border-radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'panel_radius',
		'label'       => __( 'Panel Radius ', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'default',
		'priority'    => 10,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.sidebar .panel',
				'property'      => 'border-radius',
				'units'         => 'px',
			),
			array(
				'element'       => '.sidebar .panel .panel-heading',
				'property'      => 'border-top-right-radius',
				'units'         => 'px',
			),
			array(
				'element'       => '.sidebar .panel .panel-heading',
				'property'      => 'border-top-left-radius',
				'units'         => 'px',
			),
		),
		'active_callback'  => array(
		array(
			'setting'  => 'w_setting',
			'operator' => '==',
			'value'    => 'option-2',
		),
	)
	);

	// Add a Field to change the content background color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'widg_background_color',
		'label'       => __( 'Widget background color.', 'whisk' ),
		'description' => __( 'The Widget background color specify here will apply to the sidebar widget area.', 'whisk' ),
		'section'     => 'layout',
		'default'     => '#fff',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.sidebar .widget',
				'property' => 'background-color',
			),
		),
	);

	// Add a Field to change the content background color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'widg_title_color',
		'label'       => __( 'Widget Title color.', 'whisk' ),
		'description' => __( 'The Widget background color specify here will apply to the sidebar widget area.', 'whisk' ),
		'section'     => 'layout',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.sidebar .widget h3',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the content background color
	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'widg_text_color',
		'label'       => __( 'Widget Text color.', 'whisk' ),
		'description' => __( 'The Widget background color specify here will apply to the sidebar widget area.', 'whisk' ),
		'section'     => 'layout',
		'default'     => '#000',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.sidebar .widget',
				'property' => 'color',
			),
		),
	);*/

	/*
	* Buttons etc.
	*/

	// Add a Field to change the Button LG border radius
  $fields[] = array(
    'type'        => 'number',
    'settings'    => 'button_lg_border_radius',
    'label'       => esc_attr__( 'Button LG Border Radius', 'whisk' ),
    'section'     => 'buttonsetc',
    'default'     => 6,
    'choices'     => array(
      'min'  => '0',
      'max'  => '24',
      'step' => '1',
    ),
    'output'      => array(
      array(
        'element'       => '.btn-lg',
        'property'      => 'border-radius',
        'units'         => 'px',
      ),
    ),
  );

	// Add a Field to change the Button MD border radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'button_md_border_radius',
		'label'       => esc_attr__( 'Button MD Border Radius', 'whisk' ),
		'section'     => 'buttonsetc',
		'default'     => 4,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.btn-md',
				'property'      => 'border-radius',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Button SM border radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'button_sm_border_radius',
		'label'       => esc_attr__( 'Button SM Border Radius', 'whisk' ),
		'section'     => 'buttonsetc',
		'default'     => 4,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.btn-sm',
				'property'      => 'border-radius',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Button XS border radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'button_xs_border_radius',
		'label'       => esc_attr__( 'Button XS Border Radius', 'whisk' ),
		'section'     => 'buttonsetc',
		'default'     => 2,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.btn-xs',
				'property'      => 'border-radius',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Alert border radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'alert_border_radius',
		'label'       => esc_attr__( 'Alert Border Radius', 'whisk' ),
		'section'     => 'buttonsetc',
		'default'     => 4,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.alert',
				'property'      => 'border-radius',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Panel radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'panel_border_radius',
		'label'       => esc_attr__( 'Panel Border Radius', 'whisk' ),
		'section'     => 'buttonsetc',
		'default'     => 4,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.panel',
				'property'      => 'border-radius',
				'units'         => 'px',
			),
			array(
				'element'       => '.panel-heading',
				'property'      => 'border-top-right-radius',
				'units'         => 'px',
			),
			array(
				'element'       => '.panel-heading',
				'property'      => 'border-top-left-radius',
				'units'         => 'px',
			),
		),
	);


	// Add a Field to change the Default color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'default_color',
		'label'       => __( 'Default color.', 'whisk' ),
		'description' => __( 'The Default color specified here will apply to the background of default buttons, and labels', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(

			array(
				'element' => '.btn-default, .label-default, .panel-default .panel-heading',
				'property' => 'background-color',
			),
			array(
				'element' => '.panel-default, .panel-default .panel-heading, .btn-default',
				'property' => 'border-color',
			),
		),
	);


	// Add a Field to change the Default text color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'default_text_color',
		'label'       => __( 'Default text color.', 'whisk' ),
		'description' => __( 'The Default text color specified here will apply to text all default buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-default, .label-default, .panel-default .panel-heading',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Default hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'default_hover_color',
		'label'       => __( 'Default hover color.', 'whisk' ),
		'description' => __( 'The Default color specified here will apply to the background of default buttons on hover', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(

			array(
				'element' => '.btn-default:hover',
				'property' => 'background-color',
			),
			array(
				'element' => '.btn-default:hover',
				'property' => 'border-color',
			),
		),
	);



	// Add a Field to change the Default text hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'default_text_hover_color',
		'label'       => __( 'Default text hover color.', 'whisk' ),
		'description' => __( 'The Default text color specified here will apply to text all default buttons on hover.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-default:hover',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Brand Primary color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'brand_primary_color',
		'label'       => __( 'Brand Primary color.', 'whisk' ),
		'description' => __( 'The Brand Primary color specified here will apply to the background of  primary buttons, labels, and panel headings as well as the .active class.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-primary, .label-primary, .progress-bar, .active, .panel-primary .panel-heading',
				'property' => 'background-color',
			),
			array(
				'element' => '.panel-primary, .panel-primary .panel-heading, .btn-primary',
				'property' => 'border-color',
			),
		),
	);

	// Add a Field to change the Brand Primary text color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'brand_primary_text_color',
		'label'       => __( 'Brand Primary text color.', 'whisk' ),
		'description' => __( 'The Brand Primary text color specified here will apply to text all primary buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-primary, .label-primary, .progress-bar, .panel-primary .panel-heading',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Primary hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'primary_hover_color',
		'label'       => __( 'Brand Primary hover color.', 'whisk' ),
		'description' => __( 'The Primary color specified here will apply to the background of default buttons on hover', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(

			array(
				'element' => '.btn-primary:hover',
				'property' => 'background-color',
			),
			array(
				'element' => '.btn-primary:hover',
				'property' => 'border-color',
			),
		),
	);


	// Add a Field to change the Primary text hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'primary_text_hover_color',
		'label'       => __( 'Brand Primary text hover color.', 'whisk' ),
		'description' => __( 'The Primary text color specified here will apply to text all default buttons on hover.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-primary:hover',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Success Primary color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'success_color',
		'label'       => __( 'Success color.', 'whisk' ),
		'description' => __( 'The Success color specified here will apply to the background of Success buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-success, .label-success, progress-bar-success, .panel-success .panel-heading, .alert-success',
				'property' => 'background-color',
			),
			array(
				'element' => '.panel-success, .panel-success .panel-heading, .btn-success, .alert-success',
				'property' => 'border-color',
			),
		),
	);

	// Add a Field to change the Success text color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'success_text_color',
		'label'       => __( 'Success text color.', 'whisk' ),
		'description' => __( 'The Success text color specified here will apply to text all success buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-success, .label-success, .progress-bar-success, .panel-success .panel-heading, .alert-success',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Success hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'success_hover_color',
		'label'       => __( 'Success hover color.', 'whisk' ),
		'description' => __( 'The Success color specified here will apply to the background of success buttons on hover', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(

			array(
				'element' => '.btn-success:hover',
				'property' => 'background-color',
			),
			array(
				'element' => '.btn-success:hover',
				'property' => 'border-color',
			),
		),
	);


	// Add a Field to change the Success text hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'success_text_hover_color',
		'label'       => __( 'Success text hover color.', 'whisk' ),
		'description' => __( 'The Success text color specified here will apply to text all Success buttons on hover.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-success:hover',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Info color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'info_color',
		'label'       => __( 'Info color.', 'whisk' ),
		'description' => __( 'The Info color specified here will apply to the background of Info buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-info, .label-info, progress-bar-info, .panel-info .panel-heading, .alert-info',
				'property' => 'background-color',
			),
			array(
				'element' => '.panel-info, .panel-info .panel-heading, .btn-info, .alert-info',
				'property' => 'border-color',
			),
		),
	);

	// Add a Field to change the Info text color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'info_text_color',
		'label'       => __( 'Info text color.', 'whisk' ),
		'description' => __( 'The Info text color specified here will apply to text all Info buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-info, .label-info, .progress-bar-info, .panel-info .panel-heading, .alert-info',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Info hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'info_hover_color',
		'label'       => __( 'Info hover color.', 'whisk' ),
		'description' => __( 'The Info color specified here will apply to the background of Info buttons on hover', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(

			array(
				'element' => '.btn-info:hover',
				'property' => 'background-color',
			),
			array(
				'element' => '.btn-info:hover',
				'property' => 'border-color',
			),
		),
	);


	// Add a Field to change the Info text hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'info_text_hover_color',
		'label'       => __( 'Info text hover color.', 'whisk' ),
		'description' => __( 'The Info text color specified here will apply to text all Info buttons on hover.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-info:hover',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Warning color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'warning_color',
		'label'       => __( 'Warning color.', 'whisk' ),
		'description' => __( 'The Warning color specified here will apply to the background of Warning buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-warning, .label-warning, progress-bar-warning, .panel-warning .panel-heading, .alert-warning',
				'property' => 'background-color',
			),
			array(
				'element' => '.panel-warning, .panel-warning .panel-heading, .btn-warning, .alert-warning',
				'property' => 'border-color',
			),
		),
	);

	// Add a Field to change the warning text color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'warning_text_color',
		'label'       => __( 'Warning text color.', 'whisk' ),
		'description' => __( 'The Warning text color specified here will apply to text all warning buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-warning, .label-warning, .progress-bar-warning, .panel-warning .panel-heading, .alert-warning',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Warning hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'warning_hover_color',
		'label'       => __( 'Warning hover color.', 'whisk' ),
		'description' => __( 'The Warning color specified here will apply to the background of Warning buttons on hover', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(

			array(
				'element' => '.btn-warning:hover',
				'property' => 'background-color',
			),
			array(
				'element' => '.btn-warning:hover',
				'property' => 'border-color',
			),
		),
	);


	// Add a Field to change the Warning text hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'warning_text_hover_color',
		'label'       => __( 'Warning text hover color.', 'whisk' ),
		'description' => __( 'The Warning text color specified here will apply to text all Warning buttons on hover.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-warning:hover',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Danger color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'danger_color',
		'label'       => __( 'Danger color.', 'whisk' ),
		'description' => __( 'The Danger color specified here will apply to the background of Danger buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-danger, .label-danger, progress-bar-danger, .panel-danger .panel-heading, .alert-danger',
				'property' => 'background-color',
			),
			array(
				'element' => '.panel-danger, .panel-danger .panel-heading, .btn-danger, .alert-warning',
				'property' => 'border-color',
			),
		),
	);

	// Add a Field to change the Danger text color

	$fields[] = array(
		'type'        => 'color',
		'settings'    => 'danger_text_color',
		'label'       => __( 'Danger text color.', 'whisk' ),
		'description' => __( 'The Danger text color specified here will apply to text all Danger buttons, labels, and panel headings.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-danger, .label-danger, .progress-bar-danger, .panel-danger .panel-heading, .alert-danger',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Danger hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'Danger_hover_color',
		'label'       => __( 'Danger hover color.', 'whisk' ),
		'description' => __( 'The Danger color specified here will apply to the background of Danger buttons on hover', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(

			array(
				'element' => '.btn-danger:hover',
				'property' => 'background-color',
			),
			array(
				'element' => '.btn-danger:hover',
				'property' => 'border-color',
			),
		),
	);


	// Add a Field to change the Warning text hover color

	$fields[] = array(

		'type'        => 'color',
		'settings'    => 'danger_text_hover_color',
		'label'       => __( 'Danger text hover color.', 'whisk' ),
		'description' => __( 'The Danger text color specified here will apply to text all Info buttons on hover.', 'whisk' ),
		'section'     => 'buttonsetc',
		'priority'    => 10,
		'alpha'       => true,
		'output'      => array(
			array(
				'element' => '.btn-danger:hover',
				'property' => 'color',
			),
		),
	);

	// Add a Field to change the Page header top margin
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'pageheader_margin',
		'label'       => __( 'Page Header Top Margin ', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'default',
		'priority'    => 10,
		'default'     => 0,
		'choices'     => array(
			'min'  => '0',
			'max'  => '300',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.page-header',
				'property'      => 'margin-top',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Page header height
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'pageheader_height',
		'label'       => __( 'Page Header Height ', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'default',
		'priority'    => 10,
		'default'     => 72,
		'choices'     => array(
			'min'  => '0',
			'max'  => '1170',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.page-header',
				'property'      => 'height',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Page title padding
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'pagetitle_padding',
		'label'       => __( 'Page Title Top Padding ', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'default',
		'priority'    => 10,
		'default'     => 15,
		'choices'     => array(
			'min'  => '0',
			'max'  => '1170',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => 'h1',
				'property'      => 'padding-top',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Page wrap top margin
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'pagewrap_margin',
		'label'       => __( 'Page Wrap Top Margin ', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'default',
		'priority'    => 10,
		'default'     => 15,
		'choices'     => array(
			'min'  => '0',
			'max'  => '300',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.wrap',
				'property'      => 'margin-top',
				'units'         => 'px',
			),
		),
	);

	// Add a Field to change the Archive width
	$fields[] = array(
		'type'        => 'radio-buttonset',
		'settings'    => 'archive_width',
		'label'       => __( 'Archive Width', 'whisk' ),
		'section'     => 'layout',
		'default'     => 'container',
		'priority'    => 10,
		'choices'     => array(
			'container'   => esc_attr__( 'Container - 1170px', 'whisk' ),
			'container-fluid' => esc_attr__( 'Container Fluid - 100%', 'whisk' ),
		),
	);

	// Add a Field to change the Shop width
	$fields[] = array(
		'type'        => 'radio-buttonset',
		'settings'    => 'shop_width',
		'label'       => __( 'Shop Width', 'whisk' ),
		'section'     => 'woocommerce',
		'default'     => 'container',
		'priority'    => 10,
		'choices'     => array(
			'container'   => esc_attr__( 'Container - 1170px', 'whisk' ),
			'container-fluid' => esc_attr__( 'Container Fluid - 100%', 'whisk' ),
		),
	);

	// Add a Field to change the Shop Sidebar
	$fields[] = array(
		'type'        => 'radio-buttonset',
		'settings'    => 'shop_sidebar',
		'label'       => __( 'Shop Sidebar', 'whisk' ),
		'section'     => 'woocommerce',
		'default'     => 'no-sidebar',
		'priority'    => 10,
		'choices'     => array(
			'no-sidebar'   => esc_attr__( 'No Sidebar', 'whisk' ),
			'yes-sidebar' => esc_attr__( 'Sidebar', 'whisk' ),
		),
	);

	// Add a Field to change the Sidebar Panel Colors
	$fields[] = array(
		'type'        => 'select',
		'settings'    => 'shop_sidebar_width',
		'label'       => __( 'Shop Sidebar width', 'whisk' ),
		'section'     => 'woocommerce',
		'default'     => 'quarter',
		'priority'    => 10,
		'choices'     => array(
			'quarter'   => esc_attr__( '1/4', 'my_textdomain' ),
			'third' => esc_attr__( '1/3', 'my_textdomain' ),
			'half'  => esc_attr__( '1/2', 'my_textdomain' ),
			'below'  => esc_attr__( 'Full - below', 'my_textdomain' ),
		),
		'active_callback'  => array(
		array(
			'setting'  => 'shop_sidebar',
			'operator' => '==',
			'value'    => 'yes-sidebar',
		),
	)
	);

	// Add a Field to change the Shop width
	$fields[] = array(
		'type'        => 'radio-buttonset',
		'settings'    => 'product_width',
		'label'       => __( 'Single Product Width', 'whisk' ),
		'section'     => 'woocommerce',
		'default'     => 'container',
		'priority'    => 10,
		'choices'     => array(
			'container'   => esc_attr__( 'Container - 1170px', 'whisk' ),
			'container-fluid' => esc_attr__( 'Container Fluid - 100%', 'whisk' ),
		),
	);

	// Add a Field to change the Shop Sidebar
	$fields[] = array(
		'type'        => 'radio-buttonset',
		'settings'    => 'product_sidebar',
		'label'       => __( 'Single Product Sidebar', 'whisk' ),
		'section'     => 'woocommerce',
		'default'     => 'no-sidebar',
		'priority'    => 10,
		'choices'     => array(
			'no-sidebar'   => esc_attr__( 'No Sidebar', 'whisk' ),
			'yes-sidebar' => esc_attr__( 'Sidebar', 'whisk' ),
		),
	);

	// Add a Field to change the Sidebar Panel Colors
	$fields[] = array(
		'type'        => 'select',
		'settings'    => 'product_sidebar_width',
		'label'       => __( 'Single Product Sidebar width', 'whisk' ),
		'section'     => 'woocommerce',
		'default'     => 'quarter',
		'priority'    => 10,
		'choices'     => array(
			'quarter'   => esc_attr__( '1/4', 'my_textdomain' ),
			'third' => esc_attr__( '1/3', 'my_textdomain' ),
			'half'  => esc_attr__( '1/2', 'my_textdomain' ),
			'below'  => esc_attr__( 'Full - below', 'my_textdomain' ),
		),
		'active_callback'  => array(
		array(
			'setting'  => 'product_sidebar',
			'operator' => '==',
			'value'    => 'yes-sidebar',
		),
	)
	);

	// Add a Field to change the Button LG border radius
	$fields[] = array(
		'type'        => 'number',
		'settings'    => 'woo_border_radius',
		'label'       => esc_attr__( 'WooCommerce Info Box Border Radius', 'whisk' ),
		'section'     => 'woocommerce',
		'default'     => 6,
		'choices'     => array(
			'min'  => '0',
			'max'  => '24',
			'step' => '1',
		),
		'output'      => array(
			array(
				'element'       => '.woocommerce-checkout #payment, .woocommerce form.login, .comment-form textarea, .woocommerce table.shop_table',
				'property'      => 'border-radius',
				'value_pattern' => '$px $px $px $px!important',
			),
			array(
				'element'       => '.woocommerce div.product .woocommerce-tabs ul.tabs li',
				'property'      => 'border-radius',
				'value_pattern' => '$px $px 0 0!important',
			),
		),
	);

  return $fields;
}
add_filter( 'kirki/fields', 'whisk_look_feel_fields' );
