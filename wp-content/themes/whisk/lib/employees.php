<?php


add_action( 'init', 'employees' );

function employees() {

register_post_type( 'employees', array(
  'labels' => array(
    'name' => 'Employees',
    'singular_name' => 'employees',
  ),
  'taxonomies' => array('category'),
  'description' => 'employee',
  'public' => true,
  'menu_position' => 20,
  'menu_icon'   => 'dashicons-businessman',
  'taxonomies' => array('post_tag', 'category'),
  'supports' => array( 'title', 'editor', 'custom-fields', 'paged', 'page-attributes' )
));
}

function employees_acf_add_local_field_groups() {

  acf_add_local_field_group(array(
  	'key' => 'employees_item1',
  	'title' => 'Employees',
  	'fields' => array (
  		array (
  			'key' => 'employee_image',
  			'label' => 'Employee Image',
  			'name' => 'employee_image',
  			'type' => 'image',
  		),
      array (
        'key' => 'employee_name',
        'label' => 'Employee Name',
        'name' => 'employee_name',
        'type' => 'text',
      ),
    array (
      'key' => 'employee_position',
      'label' => 'Employees Position',
      'name' => 'employee_position',
      'type' => 'text',
    ),
    array (
      'key' => 'employee_email',
      'label' => 'Employee Email',
      'name' => 'employee_email',
      'type' => 'text',
    ),
    array (
      'key' => 'employee_bio',
      'label' => 'Employee Bio',
      'name' => 'employee_bio',
      'type' => 'wysiwyg',
    ),
    array (
      'key' => 'employee_office',
      'label' => 'Employee Office location',
      'name' => 'employee_office',
      'type' => 'text',
    ),
    array (
      'key' => 'employee_office_address',
      'label' => 'Office Address',
      'name' => 'employee_office_address',
      'type' => 'text',
    ),
  ),
  	'location' => array (
  		array (
  			array (
  				'param' => 'post_type',
  				'operator' => '==',
  				'value' => 'employees',
  			),
  		),
  	),
    'position' => 'acf_after_title',
    'hide_on_screen' => array (
		0 => 'the_content',
	),
  ));

}//end function

add_action('acf/init', 'employees_acf_add_local_field_groups');


function employees_shortcode($atts) {
//ob_start();
extract(shortcode_atts(array(
    'posts' => 3,
    'cat' => '',
    'order' => 'ASC'
  ), $atts));

    $output = ' ';
    $order = get_field('employee_order');
    $loop = new WP_Query( array('post_type' => 'employees', 'posts_per_page' => $posts, 'category_name' => $cat, 'orderby' => 'menu_order', 'order' => $order ) );
    $temp_title = '';
    $temp_link = '';


    $output .= '<div id="employees-carousel" class="carousel slide" data-ride="carousel">';
    $output .= '<div class="carousel-inner" role="listbox">';

    $i = 0;
    while ( $loop->have_posts() ) : $loop->the_post();

      $img = get_field('employee_image');
      $name = get_field('employee_name');
      $position = get_field('employee_position');
      $office = trim(get_field('employee_office'));
      $address = trim(get_field('office_address'));
      //$location_name = explode('-', $locations);
      //$trimmed = trim($location_name[0]);
      $class_name = strtolower(preg_replace('/\s+/', '-', $office));

      //echo $string;
    if( $i == 0 ){
      $output .= ' <div class="item active">';
      }

     if ($i == 3 || $i == 6 || $i == 9 || $i == 12){
    $output .= '<div class="item">';
     }

    $output .= '<a class="'.$class_name.'" href="#"><div class="col-md-4">';
    $output .= '<div class="employee-img"><img class="employee-photo" src="'.$img['url'].'"></div>';
    $output .= '<div class="employee-name"><h4>'.$name.'</h4></div>';
    $output .= '<div class="employee-position"><h5>'.$position.'</h5></div>';
     //$output .= the_tags('');

    $output .= '</div></a>';

    if ($i == 2 || $i == 5 || $i == 8 || $i == 11 ){
      $output .= '</div>';
     }
    ++$i;
  endwhile;

  $output .= '</div>';
  if($posts != 3){
    $output .= '<a class="left carousel-control" href="#employees-carousel" role="button" data-slide="prev">';
    $output .= '<i class="fa fa-angle-left" aria-hidden="true"></i>';
    $output .= '<span class="sr-only">Previous</span>';
    $output .= '</a>';
    $output .= '<a class="right carousel-control" href="#employees-carousel" role="button" data-slide="next">';
    $output .= '<i class="fa fa-angle-right" aria-hidden="true"></i>';
    $output .= '<span class="sr-only">Next</span>';
    $output .= '</a>';
  }
  $output .= '</div>';

   wp_reset_query();
  //end desktop employees

  return $output;

}

add_shortcode( 'employees', 'employees_shortcode' );

add_action( 'vc_before_init', 'employees_integrateWithVC' );

function employees_integrateWithVC() {
   vc_map( array(
      "name" => __( "Employee Carousel", "my-text-domain" ),
      "base" => "employees",
      "class" => "",
      "category" => __( "Content", "my-text-domain"),
      //'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/main.js'),
      //'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/employees.scss'),
      "description" => __( "Employee 3 column carousel", "my-text-domain" ),
      "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Number of Employees", "my-text-domain" ),
            "param_name" => "posts",
            "value" => array(
              __("3") => 3,
              __("6") => 6,
              __("9") => 9,
              __("12") => 12,
            ),
            "description" => __( "Enter the number of testimonials you want in this section. Default is 6. You need 3, 6 or 9", "my-text-domain" )
         ),
         array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Category", "my-text-domain" ),
            "param_name" => "cat",
            "value" => __( "", "my-text-domain" ),
            "description" => __( "Select Specific Categories, seperated by a comma", "my-text-domain" )
         ),
         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Display Category", "my-text-domain" ),
            "param_name" => "order",
            "value" => array(
              __("ASC") => 'ASC',
              __("DESC") => 'DESC',
            ),
            "description" => __( "Order by ASC or DESC", "my-text-domain" )
         ),
      )
   ) );
}


/* Single Employee */
function employees_single_shortcode($atts) {
//ob_start();
extract(shortcode_atts(array(
    'posts' => 3,
    'cat' => '',
    'order' => 'ASC'
  ), $atts));

    $output = ' ';
    $order = get_field('employee_order');
    $loop = new WP_Query( array('post_type' => 'employees', 'posts_per_page' => $posts,  'category_name' => $cat, 'orderby' => 'menu_order', 'order' => $order ) );
    $temp_title = '';
    $temp_link = '';

$output .= '<div id="single-employee-carousel" class="carousel slide no-slide" data-ride="carousel">';
$output .= '<div class="carousel-inner" role="listbox">';

$i = 0;
while ( $loop->have_posts() ) : $loop->the_post();

      $img = get_field('employee_image');
      $name = get_field('employee_name');
      $position = get_field('employee_position');

if( $i == 0 ){
  $output .= ' <div class="item active">';
  }

if( $i > 0 ){
$output .= '<div class="item">';
}

    $output .= '<div class="col-md-12">';
    $output .= '<div class="employee-img"><img class="employee-photo" src="'.$img['url'].'"></div>';
    $output .= '<div class="employee-name"><h4>'.$name.'</h4></div>';
    $output .= '<div class="employee-position"><h5>'.$position.'</h5>'.$order.'</div>';
    $output .= '</div>';

//if ($i == 2 || $i == 5 || $i == 7 || $i == 9 || $i == 12 || $i == 15){
  $output .= '</div>';
// }
++$i;
 endwhile;

$output .= '</div>';
$output .= '<a class="left carousel-control" href="#single-employee-carousel" role="button" data-slide="prev">';
$output .= '<i class="fa fa-angle-left" aria-hidden="true"></i>';
$output .= '<span class="sr-only">Previous</span>';
$output .= '</a>';
$output .= '<a class="right carousel-control" href="#single-employee-carousel" role="button" data-slide="next">';
$output .= '<i class="fa fa-angle-right" aria-hidden="true"></i>';
$output .= '<span class="sr-only">Next</span>';
$output .= '</a>';
$output .= '</div>';

 wp_reset_query();

return $output;

}

add_shortcode( 'employees_single', 'employees_single_shortcode' );

add_action( 'vc_before_init', 'employees_single_integrateWithVC' );

function employees_single_integrateWithVC() {
   vc_map( array(
      "name" => __( "Single Employee Carousel", "my-text-domain" ),
      "base" => "employees_single",
      "class" => "",
      "category" => __( "Content", "my-text-domain"),
      //'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/main.js'),
      //'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/employees.scss'),
      "description" => __( "Single Employee carousel", "my-text-domain" ),
      "params" => array(
        array(
           "type" => "dropdown",
           "holder" => "",
           "class" => "",
           "heading" => __( "Number of Employees", "my-text-domain" ),
           "param_name" => "posts",
           "value" => array(
             __("3") => 3,
             __("6") => 6,
             __("9") => 9,
             __("12") => 12,
           ),
           "description" => __( "Enter the number of testimonials you want in this section. Default is 6. You need 3, 6 or 9", "my-text-domain" )
        ),
        array(
           "type" => "textfield",
           "holder" => "",
           "class" => "",
           "heading" => __( "Display Category", "my-text-domain" ),
           "param_name" => "cat",
           "value" => __( "", "my-text-domain" ),
           "description" => __( "Select Specific Categories, seperated by a comma", "my-text-domain" )
        ),
        array(
           "type" => "dropdown",
           "holder" => "",
           "class" => "",
           "heading" => __( "Display Category", "my-text-domain" ),
           "param_name" => "order",
           "value" => array(
             __("ASC") => 'ASC',
             __("DESC") => 'DESC',
           ),
           "description" => __( "Order by ASC or DESC", "my-text-domain" )
        ),
      )
   ) );
}

?>
