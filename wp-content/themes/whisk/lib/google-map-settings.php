<?php

function map_options() {

  acf_add_options_page(array(
		'page_title' 	=> 'Map General Settings',
		'menu_title'	=> 'Google Map',
		'menu_slug' 	=> 'map-settings',
		'capability'	=> 'edit_posts',
    'icon_url' => 'dashicons-location-alt',
		'redirect'		=> false
	));
}

function advanced_google_map_acf_add_local_field_groups() {

  acf_add_local_field_group(array(
  	'key' => 'map_settings',
  	'title' => 'Google Map',
  	'fields' => array (
      array (
        'key' => 'map_settings',
        'label' => 'Map Settings',
        'name' => 'map_settings',
        'type' => 'tab',
      ),
      array (
        'key' => 'api_key',
        'label' => 'Map API key',
        'name' => 'api_key',
        'type' => 'text',
        'wrapper' => array (
          'width' => '33%'
        ),
        'instructions' => 'To get started using the Google Maps API go to the <a href="https://console.developers.google.com/flows/enableapi?apiid=maps_backend,geocoding_backend,directions_backend,distance_matrix_backend,elevation_backend&keyType=CLIENT_SIDE&reusekey=true">Google API Console</a> to get your API key.',
      ),
      array (
        'key' => 'map_center_latitude',
        'label' => 'Map Center Latitude',
        'name' => 'map_center_latitude',
        'type' => 'text',
        'default_value' => '49.2893571',
        'wrapper' => array (
          'width' => '33%'
        ),
      ),
      array (
        'key' => 'map_center_longitude',
        'label' => 'Map Center Longitude',
        'name' => 'map_center_longitude',
        'type' => 'text',
        'default_value' => '-123.1176645',
        'wrapper' => array (
          'width' => '33%'
        ),
      ),
      array (
        'key' => 'map_zoom',
        'label' => 'Map Zoom',
        'name' => 'map_zoom',
        'type' => 'number',
        'default_value' => '14',
        'wrapper' => array (
          'width' => '33%'
        ),
      ),
      array (
        'key' => 'map_height',
        'label' => 'Map Height',
        'name' => 'map_height',
        'type' => 'number',
        'min' =>  '0',
        'default_value' => '300',
        'wrapper' => array (
          'width' => '33%'
        ),
      ),
      array (
        'key' => 'map_mobile_height',
        'label' => 'Map Mobile Height',
        'name' => 'map_mobile_height',
        'type' => 'number',
        'min' =>  '0',
        'default_value' => '150',
        'wrapper' => array (
          'width' => '33%'
        ),
      ),
      array (
        'key' => 'map_locations_tab',
        'label' => 'Map Locations',
        'name' => 'map_locations',
        'type' => 'tab',
      ),
      array (
  			'key' => 'locations',
  			'label' => 'Locations',
  			'name' => 'locations',
  			'type' => 'repeater',
        'button_label' => 'Add Location'
  		),
      array (
        'key' => 'marker',
        'label' => 'Map Marker Image',
        'name' => 'marker',
        'type' => 'image',
        'parent' => 'locations'
      ),
      array (
        'key' => 'marker_height',
        'label' => 'Map Marker Height',
        'name' => 'marker_height',
        'type' => 'number',
        'min' => '0',
        'default_value' => '40',
        'parent' => 'locations'
      ),
      array (
        'key' => 'marker_width',
        'label' => 'Map Marker Width',
        'name' => 'marker_width',
        'type' => 'number',
        'min' => '0',
        'default_value' => '22',
        'parent' => 'locations'
      ),
      array (
        'key' => 'title',
        'label' => 'Title',
        'name' => 'title',
        'type' => 'text',
        'parent' => 'locations'
      ),
      array (
        'key' => 'latitude',
        'label' => 'Latitude',
        'name' => 'latitude',
        'type' => 'text',
        'parent' => 'locations'
      ),
      array (
        'key' => 'longitude',
        'label' => 'Longitude',
        'name' => 'longitude',
        'type' => 'text',
        'parent' => 'locations',
      ),
      array (
        'key' => 'info_window',
        'label' => 'Info Window',
        'name' => 'info_window',
        'type' => 'wysiwyg',
        'parent' => 'locations',
        'wrapper' => array (
  				'width' => '33%',
  			),
      ),
  	),
  	'location' => array (
  		array (
  			array (
  				'param' => 'options_page',
  				'operator' => '==',
  				'value' => 'map-settings',
  			),
  		),
  	),
    'instruction_placement' => 'field',
    'style' => 'seamless'

  ));
}

function advanced_google_map_shortcode() {
  if (get_field('map_zoom','option')) {
    $map_zoom = get_field('map_zoom','option');
  }else {
    $map_zoom = '14';
  }
  $map_height = get_field('map_height','option');
  $map_mobile_height = get_field('map_mobile_height','option');
  $stores = array();

  // check if the repeater field has rows of data
  if( have_rows('locations', 'option') ):

   	// loop through the rows of data
      while ( have_rows('locations', 'option') ) : the_row();

          // display a sub field value
          if (get_sub_field('marker')) {
            $image = get_sub_field('marker');
            $marker = $image['url'];
          }else {
            $marker = 'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png';
          }
          $marker_height = get_sub_field('marker_height');
          $marker_width = get_sub_field('marker_width');
          $title = get_sub_field('title');
          $lat = get_sub_field('latitude');
          $lng = get_sub_field('longitude');
          $infowindow = get_sub_field('info_window');

          $store = array(
            $marker,
            $marker_width,
            $marker_height,
            $title,
            $lat,
            $lng,
            $infowindow
          );
          $stores[] = $store;
      endwhile;

  endif;

  $stores = json_encode($stores);
  ?>
  <style type="text/css" media="screen">
    #map { height: <?php echo $map_height; ?>px; }

    @media screen and (max-width: 768px) {
      #map { height: <?php echo $map_mobile_height; ?>px; }
    }
  </style>

  <div id="map-container">
    <div id="map"></div>
  </div>
  <script>

    infowindow = null;

    function initMap() {

        var myLatLng = {
            lat: <?php the_field('map_center_latitude','option'); ?>,
            lng: <?php the_field('map_center_longitude','option'); ?>
        };

        var map = new google.maps.Map(document.getElementById("map"), {
            zoom: <?php echo $map_zoom; ?>,
            center: myLatLng
        });

        setMarkers(map, stores);
        infowindow = new google.maps.InfoWindow({
            content: "loading..."
        });

        // checks to see if map is done loading and triggers a resize event to load any missing tiles
        google.maps.event.addListenerOnce(map, 'idle', function() {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(myLatLng);
            console.log(myLatLng);
        });

    }

    stores = <?php echo $stores; ?>;

    function setMarkers(map, markers) {

        for (var i = 0; i < markers.length; i++) {
            var stores = markers[i];
            var markerHeight = parseInt(stores[2]);
            var markerWidth = parseInt(stores[1]);
            var siteLatLng = new google.maps.LatLng(stores[4], stores[5]);
            var image = new google.maps.MarkerImage(stores[0], null, null, null, new google.maps.Size(markerWidth, markerHeight));
            var marker = new google.maps.Marker({
                position: siteLatLng,
                map: map,
                icon: image,
                title: stores[3],
                html: stores[6]
            });
            google.maps.event.addListener(marker, "click", function() {
                infowindow.setContent(this.html);
                infowindow.open(map, this);
            });
        }
    }

  </script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=<?php the_field('api_key','option'); ?>&callback=initMap">
  </script>
  <?php
}

function map_message_param() {
   return '<span class="vc_description vc_clearfix">To change map settings click on the <a href="admin.php?page=map-settings" title="Google Map Settings">Google Map</a> menu item on your WordPress sidebar</span>'; // This is html markup that will be outputted in content elements edit form
}

function add_advanced_google_map_to_visual_composer() {
   vc_map( array(
      "name" => __( "Advanced Google Map", "whisk" ),
      "base" => "advanced_google_map",
      "category" => __( "Content", "whisk"),
      "description" => __( "Insert an advanced google map with multiple locations and custom location markers", "whisk" ),
      "params" => array(
        array(
          "type" => "map_message",
          "holder" => "div",
          "class" => "",
          "heading" => "",
          "param_name" => "map-message"
        )
      )
    ));
}

add_action( 'init', 'map_options' );
add_action('acf/init', 'advanced_google_map_acf_add_local_field_groups');
add_shortcode( 'advanced_google_map', 'advanced_google_map_shortcode' );
vc_add_shortcode_param( 'map_message', 'map_message_param' );
add_action( 'vc_before_init', 'add_advanced_google_map_to_visual_composer' );
?>
