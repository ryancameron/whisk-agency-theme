<?php

add_action( 'init', 'portfolio' );

function portfolio() {

register_post_type( 'portfolio', array(
  'labels' => array(
    'name' => 'Portfolio',
    'singular_name' => 'Portfolio',
  ),
  'taxonomies' => array('category'),
  'description' => 'Portfolio items',
  'public' => true,
  'menu_position' => 20,
  'menu_icon'   => 'dashicons-images-alt2',
  'supports' => array( 'title', 'editor', 'custom-fields', 'paged', 'thumbnail' )
));
}

function portfolio_acf_add_local_field_groups() {

  acf_add_local_field_group(array(
  	'key' => 'portfolio_item1',
  	'title' => 'Portfolio',
  	'fields' => array (
  		array (
  			'key' => 'item_12345',
  			'label' => 'Portfolio',
  			'name' => 'portfolio',
  			'type' => 'text',
  		),
  	),
  	'location' => array (
  		array (
  			array (
  				'param' => 'post_type',
  				'operator' => '==',
  				'value' => 'portfolio',
  			),
  		),
  	),
    'position' => 'acf_after_title',
    'hide_on_screen' => array (
    0 => 'the_content',
    1 => 'custom_fields',
    2 => 'comments',
    3 => 'slug',
  ),
  ));

}//end function

add_action('acf/init', 'portfolio_acf_add_local_field_groups');

function portfolio_shortcode($atts) {

extract(shortcode_atts(array(
    'class' => ' ',
    'posts' => 6,
    'col' => 3,
  ), $atts));

if($col == 1){
  $col = 'col-md-12';
}else if ($col == 2){
  $col = 'col-md-6';
}else if ($col == 3){
  $col = 'col-md-4';
}else if($col == 4){
  $col = 'col-md-3';
}else if($col == 6){
  $col = 'col-md-2';
}
    $output = '';

    $loop = new WP_Query( array('post_type' => 'portfolio', 'posts_per_page' => $posts ) );
    $temp_title = '';
    $temp_link = '';
    while ( $loop->have_posts() ) : $loop->the_post();

      $temp_title = get_the_title();
      $temp_link = get_permalink();
      //$image = get_the_post_thumbnail('medium');
      $image = wp_get_attachment_url( get_post_thumbnail_id() );

                 $output .= '<div class="col-xs-12 col-sm-6 '. $col .' ' . $class . '">';
                 $output .= '<div class="pdf-thumb-box"><div class="pdf-thumb-box-overlay"><span class="overlay-title">';
                 $output .= $temp_title . '</span></div>';

                 $output .= '<img class"img-responsive portfolio-img" src="' . $image . '"></a></div><div class="vertical-social-box"></div></div>';
    endwhile;

 wp_reset_query();
    return $output;
}

add_shortcode( 'portfolio', 'portfolio_shortcode' );

add_action( 'vc_before_init', 'your_name_integrateWithVC' );

function your_name_integrateWithVC() {
   vc_map( array(
      "name" => __( "Portfolio", "Portfolio Pieces" ),
      "base" => "portfolio",
      "class" => "",
      "category" => __( "Content", "portfolio-pieces"),
      //'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      //'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Class", "my-text-domain" ),
            "param_name" => "class",
            "value" => array(
            __( 'Spaced Portfolio', 'js_composer' ) => "",
            __( 'No Spacing', 'js_composer' ) => "full",
          ),
            "description" => __( "Description for foo param.", "my-text-domain" )
         ),
         array(
            "type" => "textfield",
            "holder" => "",
            "class" => "",
            "heading" => __( "Posts per page", "my-text-domain" ),
            "param_name" => "posts",
            "value" => __( " ", "my-text-domain" ),
            "description" => __( "Enter the number of posts you want per page. Default is 6.", "my-text-domain" )
         ),
         array(
            "type" => "dropdown",
            "holder" => "",
            "class" => "",
            "heading" => __( "Number of Columns", "my-text-domain" ),
            "param_name" => "col",
            "value" => array(
            __( 'Default', 'js_composer' ) => 3,
            __( '1', 'js_composer' ) => 1,
            __( '2', 'js_composer' ) => 2,
            __( '3', 'js_composer' ) => 3,
            __( '4', 'js_composer' ) => 4,
            __( '6', 'js_composer' ) => 6,
          ),
            "description" => __( "Enter the number of columns.", "my-text-domain" )
         ),
      )
   )
 );// vc map array

}



/*  <div class="col-md-4 our-work-hover">
      <img src="">
      <div class="image-overlay"></div>
      <a href="">
      <div class="our-work-info">
         <h3></h3>
      </div>
     </a>
  </div> */

/*

function portfolio_shortcode( $atts ) {
$a = shortcode_atts( array(
'foo' => 'something',
'bar' => 'something else',
), $atts );

return "foo = {$a['foo']}";
}
add_shortcode( 'portfolio', 'portfolio_shortcode' );

*/

/* if( function_exists('acf_add_options_page') ) {

acf_add_options_page(array(
'page_title' 	=> 'Portfolio Settings',
'menu_title'	=> 'Portfolio Settings',
'menu_slug' 	=> 'portfolio-settings',
'capability'	=> 'edit_posts',
'redirect'		=> false
));

} */

/* add_action( 'admin_menu', 'portfolio_admin_menu' );

function portfolio_admin_menu() {
add_menu_page( 'Portfolio Settings',
'Portfolio Settings',
'manage_options',
'lib/portfolio-settings.php',
'portfolio_admin_page',
'dashicons-tickets',
6  );
} */


/* add_action('admin_menu', 'portfolio_admin_menu');

function portfolio_admin_menu(){
add_menu_page( 'Portfolio Shortcode', 'Portfolio Shortcode', 'manage_options', 'portfolio_admin_page', 'portfolio_init', '', 24 );
}

function portfolio_init(){
echo "<h1>Portfolio Shortcode</h1>";
echo "<p>This is where you find the shortcode for the portfolio pieces. Please copy and enter this shortcode into a WYSIWYG</p>";
echo "<p>The basic shortcode <strong>[portfolio]</strong></p>";
} */


?>
