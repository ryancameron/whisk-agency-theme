<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
$shop_width = get_theme_mod( 'shop_width');
$shop_sidebar = get_theme_mod('shop_sidebar');
$shop_sidebar_width = get_theme_mod('shop_sidebar_width');
$product_width = get_theme_mod('product_width');
$product_sidebar = get_theme_mod('product_sidebar');
$product_sidebar_width = get_theme_mod('product_sidebar_width');
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap  <?php if (is_post_type_archive() && $shop_width == 'container-fluid' )  {
			echo "container-fluid";
		}else if (is_product() && $product_width == 'container-fluid' )  {
			echo "container-fluid";
		}else {echo "container";}?>" role="document">
      <div class="content row">
        <main class="main col-xs-12 <?php if (is_post_type_archive() && $shop_sidebar == 'no-sidebar' )  {
					echo "col-sm-12";
				}else if (is_post_type_archive() && $shop_sidebar_width == 'quarter') {
					echo "col-sm-9";
				}else if (is_post_type_archive() && $shop_sidebar_width == 'third') {
					echo "col-sm-8";
				}else if (is_post_type_archive() && $shop_sidebar_width == 'half') {
					echo "col-sm-6";
				}else if (is_post_type_archive() && $shop_sidebar_width == 'below') {
					echo "col-sm-12";
				}else if (is_product() && $productsidebar == 'no-sidebar' )  {
					echo "col-sm-12";
				}else if (is_product() && $product_sidebar_width == 'quarter') {
					echo "col-sm-9";
				}else if (is_product() && $product_sidebar_width == 'third') {
					echo "col-sm-8";
				}else if (is_product() && $product_sidebar_width == 'half') {
					echo "col-sm-6";
				}else if (is_product() && $product_sidebar_width == 'below') {
					echo "col-sm-12";
				}?>">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar col-xs-12 <?php if (is_post_type_archive() && $shop_sidebar_width == 'quarter') {
						echo "col-sm-3";
					}else if (is_post_type_archive() && $shop_sidebar_width == 'third') {
						echo "col-sm-4";
					}else if (is_post_type_archive() && $shop_sidebar_width == 'half') {
						echo "col-sm-6";
					}else if (is_post_type_archive() && $shop_sidebar_width == 'below') {
						echo "col-sm-12";
					}if (is_product() && $product_sidebar_width == 'quarter') {
						echo "col-sm-3";
					}else if (is_product() && $product_sidebar_width == 'third') {
						echo "col-sm-4";
					}else if (is_product() && $product_sidebar_width == 'half') {
						echo "col-sm-6";
					}else if (is_product() && $product_sidebar_width == 'below') {
						echo "col-sm-12";
					}?>">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
