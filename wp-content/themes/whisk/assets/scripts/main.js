/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        $(document).ready(function() {

            if( $('.desktop .navbar-default').hasClass('navbar-fixed-top') ){

              $(window).scroll(function() {
                  if ($(this).scrollTop() > 40){
                  $('.desktop .navbar').addClass("sticky");
                  }else{
                  $('.desktop .navbar').removeClass("sticky");
                  }
              });

            }

										
          // you want to enable the pointer events only on click;
          $('#map').addClass('scrolloff'); // set the pointer events to none on doc ready
          $('#map-container').on('click', function () {
              $('#map').removeClass('scrolloff'); // set the pointer events true on click
          });
          // you want to disable pointer events when the mouse leaves the canvas area;
          $("#map").mouseleave(function () {
              $('#map').addClass('scrolloff'); // set the pointer events to none when mouse leaves the map area
          });

          if($('.navbar-header').hasClass('logo-center')) {
            $height = $('.navbar-brand').outerHeight(true);
            $('.navbar-center').css('margin-top', $height);
          }

          $('[data-toggle="dropdown"]').click(function() {
              location.href = $(this).attr('href');
          });




            $('.dropdown').mouseenter(function() {
                $( '> .dropdown-menu', this).fadeIn(300);
            });

            $('.dropdown').mouseleave(function() {
              $( '> .dropdown-menu', this).fadeOut(300);
            });

            $('#employees-carousel .carousel-inner > .item').matchHeight({
              byRow: false,
            });

            $('#single-employee-carousel .carousel-inner > .item').matchHeight({
              byRow: false,
            });

            $('#testimonials-carousel .carousel-inner > .item').matchHeight({
              byRow: false,
            });

            $('.carousel.no-slide').carousel({
              interval: false
            });

            $('#employees-carousel').carousel({
              interval: false
            });

        });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
