<div class="alert alert-danger">
	<h2>404</h2>
	<h3>Page Not Found</h3>
  <p>
		<?php _e('Sorry, but the page you were trying to view does not exist.<br />Try searching for the page you are looking for.', 'sage'); ?>
	</p>
	<div><br />
		<?php get_search_form(); ?>
	</div>
</div>
