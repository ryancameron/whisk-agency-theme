
<div class="desktop desktop-header">
<header class="banner navbar navbar-default <?php echo get_theme_mod('sticky_header'); ?> <?php if(get_theme_mod('header_border_toggle') == 'no-border') : echo get_theme_mod('header_border_toggle'); endif; ?> <?php echo get_theme_mod('logo_position', 'whisk-navbar-left'); ?>" role="banner">
  <div class="pre-header <? echo get_theme_mod('header_width', 'container'); ?> <? echo get_theme_mod('pre_header_toggle', 'hide-pre-header'); ?>">
      <?php get_template_part('templates/pre_header'); ?>
  </div>
  <div class="<? echo get_theme_mod('header_width', 'container'); ?>">
    <div class="navbar-header desktop">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php
        $image = attachment_url_to_postid(get_theme_mod( 'logo_image' ));
        $logo_src = wp_get_attachment_image_src($image, 'full')[0];
        $src = get_theme_mod( 'logo_image' );
        $sticky = attachment_url_to_postid(get_theme_mod( 'sticky_logo_image' ));
        $sticky_logo = wp_get_attachment_image_src($sticky, 'full')[0];
        $mobile = attachment_url_to_postid(get_theme_mod( 'mobile_logo_image' ));
        $mobile_logo = wp_get_attachment_image_src($mobile, 'full')[0];
        $logo_padding = get_theme_mod('logo_padding');
        $logoPos = get_theme_mod('logo_position', 'whisk-navbar-left');
        $blogName = get_bloginfo( 'name' );
        $url = esc_url(home_url('/'));
        //echo $logoPos;
        if($logoPos == 'branding-center'){
          echo '</div>';
          echo '<nav class="desktop" role="navigation">';
          echo '  <div class="centered-logo"><div class="inner-logo"><a class="navbar-brand" href="'. $url  .'"><img class="desktop-logo" src="'. $logo_src .'" alt="'. $blogName .'"></a></div></div>';
          echo '  <div class="centered-logo"><div class="inner-logo"><a class="navbar-brand" href="'. $url  .'"><img class="sticky-logo" src="'. $sticky_logo . '" alt="'. $blogName .' "></a></div></div>';
            wp_nav_menu([
            'theme_location' => 'left_nav',
            'walker' => new wp_bootstrap_navwalker(),
            'menu_class' => 'nav navbar-nav nav-left'
            ]);

            wp_nav_menu([
            'theme_location' => 'right_nav',
            'walker' => new wp_bootstrap_navwalker(),
            'menu_class' => 'nav navbar-nav nav-right'
          ]);
          echo '</nav>';
        }else{
      ?>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
        <?php
        if($image){ ?>
          <img class="desktop-logo" src="<?php echo $logo_src; ?>" alt="<?php bloginfo( 'name' ); ?>" />
  <?php }else{
          echo get_bloginfo('name');
        }
      if($sticky){ ?>
        <img class="sticky-logo" src="<?php echo $sticky_logo; ?>" alt="<?php bloginfo( 'name' ); ?>" />
  <?php } ?>
      </a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu([
          'theme_location' => 'primary_navigation',
          'walker' => new wp_bootstrap_navwalker(),
          'menu_class' => 'nav navbar-nav '
        ]);
      endif;
      //get_theme_mod('header_menu_position')
      echo '</nav>';
     } ?>
   </div>
   <div class="post-header <? echo get_theme_mod('header_width', 'container'); ?> <? echo get_theme_mod('post_header_toggle', 'hide-post-header'); ?>">
       <?php get_template_part('templates/post_header'); ?>
   </div>
 </header>

</div><!-- desktop -->

<div class="mobile">
  <header class="banner navbar navbar-default <?php echo get_theme_mod('mobile_sticky_header'); ?>" role="banner">
    <div class="pre-header <? echo get_theme_mod('header_width', 'container'); ?> <? echo get_theme_mod('pre_header_toggle', 'hide-pre-header'); ?>">
        <?php get_template_part('templates/pre_header'); ?>
    </div>
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar"><!-- mobile-menu -->
          <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
  <a class="mobile navbar-brand" href="<?= esc_url(home_url('/')); ?>">
    <?php if($mobile){ ?>
     <img class="mobile-logo" src="<?php echo $mobile_logo; ?>" alt="<?php bloginfo( 'name' ); ?>" />
   <?php } ?>
   </a>
</div> <!-- navbar-header-->
<div class="collapse navbar-collapse" id="bs-navbar"><!-- mobile-menu -->
    <?php
    //if (has_nav_menu('mobile_nav')) :
      wp_nav_menu([
        'theme_location' => 'mobile_nav',
        'walker' => new wp_bootstrap_navwalker(),
        'menu_class' => 'nav navbar-nav '
      ]);
  //  endif; ?>
</div>
  </div>
  <div class="post-header <? echo get_theme_mod('header_width', 'container'); ?> <? echo get_theme_mod('post_header_toggle', 'hide-post-header'); ?>">
      <?php get_template_part('templates/post_header'); ?>
  </div>
</header>
</div><!-- end mobile -->
