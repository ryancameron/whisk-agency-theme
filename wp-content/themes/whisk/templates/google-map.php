<?php /*
****
  DEPRECATED. The map code can be found in the google_map_shortcode() function within google-map-settings.php
****
*/?>
<div id="map-container">
  <div id="map"></div>
</div>
<script>
  function initMap() {
    stores = <?php echo $stores; ?>;

    var myLatLng = {lat: <?php the_field('map_center_latitude','option'); ?>, lng: <?php the_field('map_center_longitude','option'); ?>};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: parseFloat('<?php the_field('map_zoom','option'); ?>'),
      center: myLatLng
    });

    for(i = 0; i < stores.length; i++) {
      var data = stores[i],
          latLng = new google.maps.LatLng(data[2], data[3]);
      var image = new google.maps.MarkerImage(data[0], null, null, null, new google.maps.Size(40,70));
      var contentString = data[4];

      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      console.log(infowindow);
      // Creating a marker and putting it on the map
      var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        title: data[1],
        icon: image,
        optimized: false,
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });
    }
  }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=<?php the_field('map_api_field','option'); ?>&callback=initMap">
</script>
