<footer class="content-info">
  <div class="pre-footer <? echo get_theme_mod('pre_footer_toggle', 'hide-pre-footer'); ?>">
		<div class="<? echo get_theme_mod('footer_width', 'container'); ?> ">
			<?php get_template_part('templates/pre_footer'); ?>
		</div>
  </div>
  <div class="<? echo get_theme_mod('footer_container','container'); ?>">
    <div class="row">
      <?php
        $footer = get_theme_mod( 'footer_grid','option-1');
        if ( $footer == 'option-1'){ ?>

         <div class="col-xs-12 col-sm-12 col-md-12">
           <?php dynamic_sidebar('footer-1'); ?>
         </div>

      <?php }else if($footer == 'option-2') { ?>

        <div class="col-xs-12 col-sm-12 col-md-6">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>
      <?php }else if($footer == 'option-3') { ?>

        <div class="col-xs-12 col-sm-12 col-md-4">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
          <?php dynamic_sidebar('footer-3'); ?>
        </div>
      <?php } else if($footer == 'option-4') { ?>

        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php dynamic_sidebar('footer-3'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php dynamic_sidebar('footer-4'); ?>
        </div>

      <?php } else if($footer == 'option-6') { ?>

        <div class="col-xs-12 col-sm-12 col-md-2">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2">
          <?php dynamic_sidebar('footer-3'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2">
          <?php dynamic_sidebar('footer-4'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2">
          <?php dynamic_sidebar('footer-5'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2">
          <?php dynamic_sidebar('footer-6'); ?>
        </div>
      <?php } else if($footer == 'option-7') { ?>

        <div class="col-xs-12 col-sm-12 col-md-8">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>

      <?php } else if($footer == 'option-8') { ?>

        <div class="col-xs-12 col-sm-12 col-md-4">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>

      <?php } else if($footer == 'option-9') { ?>

        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-9">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>

      <?php } else if($footer == 'option-10') { ?>

        <div class="col-xs-12 col-sm-12 col-md-9">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>

      <?php } ?>

    </div>
    <nav class="footer-nav row <? echo get_theme_mod('footer_menu_toggle', 'hide-footer-menu'); echo ' ' . get_theme_mod('footer_menu_position'); ?>" role="navigation">
      <?php
      if (has_nav_menu('footer_navigation')) :
        wp_nav_menu([
          'theme_location' => 'footer_navigation',
          'menu_class' => 'nav navbar-nav'
        ]);
      endif;
      ?>
    </nav>
  </div>
  <div class="post-footer <? echo get_theme_mod('post_footer_toggle', 'hide-post-footer'); ?>">
		<div class="<? echo get_theme_mod('footer_width', 'container'); ?>">
			<?php get_template_part('templates/post_footer'); ?>
		</div>  
  </div>
</footer>
