<?php use Roots\Sage\Titles; ?>

<?php $stickyTemplate = get_field('sticky_template_toggle');

if($stickyTemplate != 'sticky-template'){

    $toggle = get_field('page_title_toggle');
    if($toggle == 'on'){ ?>

      <div class="page-header" <?php

      $image = get_field('page_header_bg_img');

      if( !empty($image) ): ?> style="background:	url(<?php echo $image['url']; ?>); background-position: center; background-size: cover;" <?php endif; ?> />
        <h1><?= Titles\title(); ?></h1>
      </div>

    <?php }

}    ?>
