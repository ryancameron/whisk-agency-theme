<div class="row">
  <?php
  $post_header_grid = get_theme_mod( 'post_header_grid','option-1');

  if ( $post_header_grid == 'option-1'){
  ?>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php dynamic_sidebar('post-header-1'); ?>
    </div>

  <?php }else if($post_header_grid == 'option-2') { ?>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <?php dynamic_sidebar('post-header-1'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <?php dynamic_sidebar('post-header-2'); ?>
    </div>

  <?php }else if($post_header_grid == 'option-3') { ?>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <?php dynamic_sidebar('post-header-1'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-12">
      <?php dynamic_sidebar('post-header-2'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-12">
      <?php dynamic_sidebar('post-header-3'); ?>
    </div>

  <?php } else if($post_header_grid == 'option-4') { ?>

    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <?php dynamic_sidebar('post-header-1'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <?php dynamic_sidebar('post-header-2'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <?php dynamic_sidebar('post-header-3'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <?php dynamic_sidebar('post-header-4'); ?>
    </div>

  <?php } else if($post_header_grid == 'option-6') { ?>

    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <?php dynamic_sidebar('post-header-1'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <?php dynamic_sidebar('post-header-2'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <?php dynamic_sidebar('post-header-3'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <?php dynamic_sidebar('post-header-4'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <?php dynamic_sidebar('post-header-5'); ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <?php dynamic_sidebar('post-header-6'); ?>
    </div>

  <?php } ?>

</div>
