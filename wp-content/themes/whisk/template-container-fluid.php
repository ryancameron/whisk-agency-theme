<?php
/**
 * Template Name: Container Fluid
 */
?>

<section class="container-fluid">
	<div class="row">
		<?php while (have_posts()) : the_post(); ?>
		  <?php get_template_part('templates/content', 'page'); ?>
		<?php endwhile; ?>
	</div>
</section>
