<section class="container">
	<div class="row">
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
			<hr/>
			<h2>Header</h2>
			<hr />
			<p>
				The Header area in Whisk is comprised of three sections, a Pre Header section, a Header section, and a Post Header section.</p>
				<p>
					The Pre & Post Header section are widget areas - These can be styled using the customizer under Appearance/Customize/Header/Pre Header and Appearance/Customize/Header/Post Header respectively. Each of these areas can have 1, 2, 3, 4 or 6 columns.
				</p>
				<p>
					The Header section has several options that can be configured under Appearance/Customize/Header/Header options
				</p>
				<p>
						These include:
				</p>
				<ul>
					<li>
						Sticky header
					</li>
					<li>
						Header Width
					</li>
					<li>
						Brand/Logo position
					</li>
					<li>
						Logo/Brand padding
					</li>
					<li>
						Header Background Color
					</li>
					<li>
						Header Background Image
					</li>
					<li>
						 Header Elements Height - This controls the height of nav elements and Logo/Brand
					</li>
					<li>
						Toggle Header Bottom Border
					</li>
				</ul>
				<p>
					The Header Menu area has several options that can be configured under Appearance/Customize/Header/Header Menu options
				</p>
				<p>
					These include:
				</p>
				<ul>
					<li>
						Menu Position - Left, Center or Right
					</li>
					<li>
						Menu Typography
					</li>
					<li>
						Menu Item Text Hover Color
					</li>
					<li>
						Menu Item Background Hover Color
					</li>
					<li>
						Active Menu Item Text Color
					</li>
					<li>
						Active Menu Item Background Color
					</li>
				</ul>
				<hr />
				<h2>Typography</h2>
				<hr />
				<h3>H tags</h3>
				<p>The Settings for H tag fonts and colors can be found in Appearance/Customize/Look & Feel/Typography</p>
				<h1>This is an H1 Heading</h1>
				<p>H1 tags should be used for the page title.  </p>
				<h2>This is an H2 Heading</h2>
				<p>H2 tags should be used for subheadings</p>
				<h3>This is an H3 Heading</h3>
				<p>H3 tags should be used for subheadings and widget and card titles</p>
				<h4>This is an H4 Heading</h4>
				<p>H4 tags should be used for sub-subheading, widget and card titles</p>
				<h5>This is an H5 Heading</h5>
				<p>H5 tags should be used for Footer titles</p>
				<h6>This is an H6 Heading</h6>
				<p>H6 tags should be used for Footer titles</p>

				<h3>Body Text</h3>
				<p>Body text can be styled using the customizer under Appearance/Customize/Look & Feel/Typography
				</p>
				<h3>Links</h3>
				<p>
					<a>This is a link</a> - Links can be styled using the customizer under Appearance/Customize/Look & Feel/Typography
				</p>
				<hr />
				<h2>WYSIWYG Widgets / Widget Blocks</h2>
				<hr />
				<p>
					WYSIWYG Widgets / Widget Blocks is a plugin that can be activate to create Widget blocks using the Wordpress WYSIWIG this is the best way to add complex content to Widget areas such as the Pre/Post Header, Pre/Post Footer and the Sidebar.
				</p>
				<hr />
				<h2>Page layouts / Visual Composer / Sidebars</h2>
				<hr />
					<p>
						Whisk includes several page layouts that can be utilized for different use cases. In addition each page has a page width option which can be used to select Full Width or Containerized content. We have also included Visual Composer in the theme. Most pages should be created using Visual Composer. For most pages that are built using Visual Composer the page width should be set to Full.
					</p>
					<p>
						These are:
					</p>
					<ul>
						<li>
							Full Width No Sidebar - This is the template that should be used for most pages that are being built with Visual Composer.
						</li>
						<li>
							Full Width Sidebar Below - This creates a Full Width page with a Sidebar Widget area below the main content.
						</li>
						<li>
							50/50 - This template creates a page with a content area at 50%, and a sidebar area at 50%, This should only be used in special instances
						</li>
						<li>
							2/3 - 1/3 - This template creates a page with a content area that is 2/3 wide and a sidebar that is 1/3. This should be used on pages that must have an aside area that is seperate from the Visual Composer content. For example a site that has repeated sidebar on many pages may suit using this template.
						</li>
						<li>
						3/4 - 1/4 - This template creates a page with a content area that is 3/4 wide and a sidebar that is 1/4. This should be used on pages that must have an aside area that is seperate from the Visual Composer content. For example a site that has repeated sidebar on many pages may suit using this template.
						</li>
						<li>
							Splash Page - This template creates a page that does not have a header or footer area and should only be used in special cases. EG a temporary landing page.
						</li>
					</ul>
				<hr />
				<h2>Portfolio</h2>
				<hr />
				<hr />
				<h2>Maps</h2>
				<hr />
				<p>
					Maps ...
				</p>
				<p>
					Maps can be styled using the included Snazzy Maps plugin.
				</p>
				<hr />
				<h2>Font Awesome</h2>
				<hr />
				<p>
					Font Awesome icons can be included by giving an i or span element the class fa followed by the desired icon class. For example an icon with the classes fa fa-bullseye would look like this: <i class="fa fa-bullseye"></i>
				</p>
				<p>
					Lists can also be styled by adding the class fa-ul to the ul element and fa-li followed by the desired icon class to li elements. For example a list with the style fa-li fa-check-square would look like this:
				</p>
				<ul class="fa-ul">
					<li class="fa-li fa fa-check-square"> 1</li><br />
					<li class="fa-li fa fa-check-square"> 2</li><br />
					<li class="fa-li fa fa-check-square"> 3</li><br />
				</ul>
				<p>
					For more information on advanced icon usage visit the <a href="http://fontawesome.io/examples/">Font Awesome example page</a>
				</p>
				<hr />
				<h2 class="animated infinite tada">Animations</h2>
				<hr />
				<p>
					The theme also includes <a href="https://github.com/daneden/animate.css">Animate.css</a> for easy use of animations. These animations can be triggered with html classes or with jQuery. For use in CSS simply give your object a class of animated and followed by the type of animation. The Title above has the class "animated infinite shake".
				</p>
				<hr />
				<h2>WooCommerce</h2>
				<hr />
				<p>
					For eCommerce sites we have included the WooCommerce plugin and basic styling of WooCommerce pages and elements. Whisk has several several WooCommerce options that can be configured under Appearance/Customize/Advanced/WooCommerce options. These include:
				</p>
				<ul>
					<li>
						Shop Width - This sets the width of the shop page, it can be either Full Width or in a Container
					</li>
					<li>
						Shop Sidebar - This determines whether the shop page has a sidebar or not.
					</li>
					<li>
						Single Product Width - This determines the width of products. They can be Full Width or in a Container.
					</li>
					<li>
						Single Product Sidebar - This determines whether the single products have a sidebar or not.
					</li>
					<li>
						WooCommerce Info Box Border Radius - This determines the border radius for WooCommerce info boxes as well as Cart/Checkout items
					</li>
				</ul>
				<hr />
				<h2>Plugins</h2>
				<hr />
				<p>
					You may also want to activate some of the other plugins included in this package. These include:
				</p>
				<ul>
					<li>
						ACF-Content Analysis for Yoast SEO - Allows Yoast SEO to analyze content entered in Advanced Custom Fields
					</li>
					<li>
						Contact Form 7 - Use this plugin for simple contact formats
					</li>
					<li>
						If Menu - Conditional logic for menu items
					</li>
					<li>
						Widget CSS Classes - Add classes to your widgets so that they can be styled using CSS
					</li>
					<li>
						Widget Logic - Conditional logic for placement of widgets (eg only on home, or only on posts)
					</li>
				</ul>


			<hr />
			<h2>Button Styles</h2>
			<hr />
			<p>
				Button styles can be styled using the customizer under Appearance/Customize/Look & Feel/Buttons, Alerts, Labels
			</p>
			<div class="row">
			<div class="col-xs-12 col-sm-4">
				<div class="sg-btn">
					<btn class="btn btn-lg btn-default">Default Button LG</btn><br />
					<span>To use this style, give your button the classes btn btn-lg btn-default</span><br />
				</div>
				<div class="sg-btn">
				<btn class="btn btn-md btn-default">Default Button MD</btn><br />
				<span>To use this style, give your button the classes btn btn-md btn-default</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-sm btn-default">Default Button SM</btn><br />
				<span>To use this style, give your button the classes btn btn-sm btn-default</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-xs btn-default">Default Button XS</btn><br />
				<span>To use this style, give your button the classes btn btn-xs btn-default</span><br />
			</div>
			</div>

			<div class="col-xs-12 col-sm-4">
			<div class="sg-btn">
				<btn class="btn btn-lg btn-primary">Primary Button LG</btn><br />
				<span>To use this style, give your button the classes btn btn-lg btn-primary</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-md btn-primary">Primary Button MD</btn><br />
				<span>To use this style, give your button the classes btn btn-md btn-primary</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-sm btn-primary">Primary Button SM</btn><br />
				<span>To use this style, give your button the classes btn btn-sm btn-primary</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-xs btn-primary">Primary Button XS</btn><br />
				<span>To use this style, give your button the classes btn btn-xs btn-primary</span><br />
			</div>
			</div>

			<div class="col-xs-12 col-sm-4">

			<div class="sg-btn">
				<btn class="btn btn-lg btn-success">Success Button LG</btn><br />
				<span>To use this style, give your button the classes btn btn-lg btn-success</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-md btn-success">Success Button MD</btn><br />
				<span>To use this style, give your button the classes btn btn-md btn-success</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-sm btn-success">Success Button SM</btn><br />
				<span>To use this style, give your button the classes btn btn-sm btn-success</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-xs btn-success">Success Button XS</btn><br />
				<span>To use this style, give your button the classes btn btn-xs btn-success</span><br />
			</div>

			</div>

			<div class="col-xs-12 col-sm-4">

			<div class="sg-btn">
				<btn class="btn btn-lg btn-info">Info Button LG</btn><br />
				<span>To use this style, give your button the classes btn btn-lg btn-info</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-md btn-info">Info Button MD</btn><br />
				<span>To use this style, give your button the classes btn btn-md btn-info</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-sm btn-info">Info Button SM</btn><br />
				<span>To use this style, give your button the classes btn btn-sm btn-info</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-xs btn-info">Info Button XS</btn><br />
				<span>To use this style, give your button the classes btn btn-xs btn-info</span><br />
			</div>
			</div>

			<div class="col-xs-12 col-sm-4">

			<div class="sg-btn">
				<btn class="btn btn-lg btn-warning">Warning Button LG</btn><br />
				<span>To use this style, give your button the classes btn btn-lg btn-warning</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-md btn-warning">Warning Button MD</btn><br />
				<span>To use this style, give your button the classes btn btn-md btn-warning</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-sm btn-warning">Warning Button SM</btn><br />
				<span>To use this style, give your button the classes btn btn-sm btn-warning</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-xs btn-warning">Warning Button XS</btn><br />
				<span>To use this style, give your button the classes btn btn-xs btn-warning</span><br />
			</div>
			</div>

			<div class="col-xs-12 col-sm-4">

			<div class="sg-btn">
				<btn class="btn btn-lg btn-danger">Danger Button LG</btn><br />
				<span class="sg-btn-label">To use this style, give your button the classes btn btn-lg btn-danger</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-md btn-danger">Danger Button MD</btn><br />
				<span class="sg-btn-label">To use this style, give your button the classes btn btn-md btn-danger</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-sm btn-danger">Danger Button SM</btn><br />
				<span class="sg-btn-label">To use this style, give your button the classes btn btn-sm btn-danger</span><br />
			</div>
			<div class="sg-btn">
				<btn class="btn btn-xs btn-danger">Danger Button XS</btn><br />
				<span class="sg-btn-label">To use this style, give your button the classes btn btn-xs btn-danger</span><br />
			</div>
			</div>
				</div>
			<hr></hr>
			<h2>Labels</h2>
			<hr />
			<div>
				<div class="sg-labels">
					<span class="label label-default">Default</span><span> - use class label label-default to use this styling </span><br />
				</div>
				<div class="sg-labels">
					<span class="label label-primary">Primary</span><span> - use class label label-primary to use this styling </span><br />
				</div>
				<div class="sg-labels">
					<span class="label label-success">Success</span><span> - use class label label-success to use this styling </span><br />
				</div>
				<div class="sg-labels">
					<span class="label label-info">Info</span><span> - use class label label-info to use this styling </span><br />
				</div>
				<div class="sg-labels">
					<span class="label label-warning">Warning</span><span> - use class label label-warning to use this styling </span><br />
				</div>
				<div class="sg-labels">
					<span class="label label-danger">Danger</span><span> - use class label label-danger to use this styling </span><br />
				</div>
			</div>
			<hr />
			<h2>Alerts</h2>
			<hr />
			<div class="alert alert-success" role="alert">Alert Success - give your div a class of alert-success for this effect</div>
			<div class="alert alert-info" role="alert">Alert Info - give your div a class of alert-info for this effect</div>
			<div class="alert alert-warning" role="alert">Alert Warning - give your div a class of alert-warning for this effect</div>
			<div class="alert alert-danger" role="alert">Alert Danger - give your div a class of alert-danger for this effect</div>
			<hr />
			<h2>Panels</h2>
			<p>
				Panel styles can changed via the customizer at Appearance/Customize/
			</p>
			<div class="panel panel-default">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Panel title</h3>
		  	</div>
		  	<div class="panel-body">
		    	Panel content - Give your content a class of panel panel-default to utilize this styling. Note that your panel title need to be wrapped in a div with the class panel-heading and panel content needs to be wrapped in a div with the class panel-body.
		  	</div>
			</div>
			<div class="panel panel-primary"><div class="panel-heading">
				<h3 class="panel-title">Panel Brand Primary</h3>
			</div>
			<div class="panel-body">
				Panel content - Give your content a class of panel panel-default to utilize this styling. Note that your panel title need to be wrapped in a div with the class panel-heading and panel content needs to be wrapped in a div with the class panel-body.
			</div></div>
			<div class="panel panel-success"><div class="panel-heading">
				<h3 class="panel-title">Panel Success</h3>
			</div>
			<div class="panel-body">
				Panel content - Give your content a class of panel panel-default to utilize this styling. Note that your panel title need to be wrapped in a div with the class panel-heading and panel content needs to be wrapped in a div with the class panel-body.
			</div></div>
			<div class="panel panel-info"><div class="panel-heading">
				<h3 class="panel-title">Panel Info</h3>
			</div>
			<div class="panel-body">
				Panel content - Give your content a class of panel panel-default to utilize this styling. Note that your panel title need to be wrapped in a div with the class panel-heading and panel content needs to be wrapped in a div with the class panel-body.
			</div></div>
			<div class="panel panel-warning"><div class="panel-heading">
				<h3 class="panel-title">Panel Warning</h3>
			</div>
			<div class="panel-body">
				Panel content - Give your content a class of panel panel-default to utilize this styling. Note that your panel title need to be wrapped in a div with the class panel-heading and panel content needs to be wrapped in a div with the class panel-body.
			</div></div>
			<div class="panel panel-danger"><div class="panel-heading">
				<h3 class="panel-title">Panel Danger</h3>
			</div>
			<div class="panel-body">
				Panel content - Give your content a class of panel panel-default to utilize this styling. Note that your panel title need to be wrapped in a div with the class panel-heading and panel content needs to be wrapped in a div with the class panel-body.
			</div></div>
		</div>
			<hr />
			<h2>Footer</h2>
			<hr />
			<p>
				Like the Header, the footer area in Whisk is comprised of three sections, a Pre Footer section, a Footer section, and a Post Footer section.</p>
				<p>
					The Pre & Post Footer sections are widget areas - These can be styled using the customizer under Appearance/Customize/Footer/Pre Footer and Appearance/Customize/Header/Post Footer respectively. Each of these areas can have 1, 2, 3, 4 or 6 columns.
				</p>
				<p>
					The Footer section is also widgetized and can have 1, 2, 3, 4 or 6 columns. The Footer has several options that can be configured under Appearance/Customize/Footer/Footer options.
				</p>
				<p>
						These include:
				</p>
				<ul>
					<li>
						Footer Width
					</li>
					<li>
						Footer Grid
					</li>
					<li>
						Footer Background Color
					</li>
					<li>
						Footer Heading Color
					</li>
					<li>
						Footer Text Color
					</li>
					<li>
						Footer Link Color
					</li>
					<li>
						Footer Link Hover Color
					</li>
					<li>
						Footer Link Hover Decoration
					</li>
					<li>
						Footer Text Align
					</li>
					<li>
						Footer Padding
					</li>
					<li>
						Footer Top Margin
					</li>
				</ul>
			<?php endwhile; ?>
	</div>
</section>
